-- Master NPV Performance Pull
-- Last Updated 07/17/2019
-- Last Updated by Drayton Thomas

SELECT    
       --stuff from the mail table
	   d.Campaign_ID 
	   ,year(p.AT_ACCOUNT_OPEN_DT)*100 + MONTH(p.AT_ACCOUNT_OPEN_DT) as appmonth
	   ,str(YEAR(p.AT_ACCOUNT_OPEN_DT)) + 'Q' + ltrim(str(CEILING(cast(month(p.AT_ACCOUNT_OPEN_DT)as decimal)/3))) as appqtr
	   ,p.[AT_ACCOUNTID] 
	   ,d.OFFER_ID
	   ,p.[AT_MONTHSONBOOKS] as MOB	
	   ,p.[AT_PERIODID] as cal_month

	   --stuff from application decisions table
	   ,d.product_nm -- product: Platinum/Rewards/Optimum
	   ,d.FDR_PURCH_PROMO_IN -- purchase promo flag; 'A' is no promo
	   ,d.FDR_BT_PROMO_APR_IN -- BT promo flag; 'A' is no promo 
	   ,d.FDR_ANNUAL_FEE_IN -- annual fee flag; 'A' is no fee
	   ,d.RISK_SCORE_1 as App_LT_Ntile -- Gen 1 LT model (hard cut in Experian Right Offer)
	   ,d.RISK_SCORE_2 as App_ST_Ntile -- Gen 1 ST model (secondary grounding dimension for PV models)

	   -- Gen 2 LT model: combining production & retro-scores to improve coverage
	   ,case   
		  when d.[Cust_Mod_3_Fin_Scr_Dec] in ('000', '448', '00') or d.Cust_Mod_3_Fin_Scr like '%y%' or d.Cust_Mod_3_Fin_Scr = 'N000000000000000000' or d.Cust_Mod_3_Fin_Scr = 'Y000000000000000000' or d.Cust_Mod_3_Fin_Scr is null then 
			case 
				when ms.pred_gen2_1 > 0 then
					case when ms.pred_gen2_1 < 0.19 then ceiling(100*ms.pred_gen2_1) else 20 end
			else 99
			end
		  when cast(d.Cust_Mod_3_Fin_Scr as float) > 20 then 20
		  when cast(d.Cust_Mod_3_Fin_Scr as float) = 0 then 99
			else ceiling(cast(d.Cust_Mod_3_Fin_Scr as float))
		end as lt2_ventile

		-- TU LT Model: for use in Credit Karma; combining production & retro-scores to improve coverage; retro-scores only go back through Jan '18
	  ,case 
			when d.Cust_Mod_7_Fin_Scr is null or d.Cust_Mod_7_Fin_Scr = 'N000000000000000000' or d.Cust_Mod_7_Fin_Scr = 'Y000000000000000000' then 
				case when cktu.probability_1 > 0 and cktu.probability_1 <= 1 then case when cktu.probability_1 > 0.2 then 20 else CEILING(100*cktu.probability_1) end
					else 99
				end
			when cast(d.Cust_Mod_7_Fin_Scr as float) > 20 then 20
			when cast(d.Cust_Mod_7_Fin_Scr as float) = 0 then 99
			else ceiling(cast(d.Cust_Mod_7_Fin_Scr as float))
		end as tu_lt

		-- Marketing Stage Gen 2 LT Model: for use in DM campaigns (currently just using for LT/util targeting grid)
	  ,case when mkt.mktg_LT is null or mkt.mktg_LT = 0 then 99 
			else mkt.mktg_LT 
		end as mktg_LT

		-- ST3 model: have to combine production & two separate retro-score tables
	  ,case when d.cust_mod_5_fin_scr = 'N000000000000000000' or d.cust_mod_5_fin_scr = 'Y000000000000000000' then 99
		when d.cust_mod_5_fin_scr is not null and cast(d.cust_mod_5_fin_scr as float) > 0 then 
			case when cast(d.cust_mod_5_fin_scr as float) >= 10 then 10
			when cast(d.cust_mod_5_fin_scr as float) = 0 then 99
			else ceiling(cast(d.cust_mod_5_fin_scr as float))
			end
		when ms2.st_g3 is not null and ms2.st_g3 > 0 then
			case when ms2.st_g3 >= 0.1 then 10
			when ms2.st_g3 = 0 then 99
			else ceiling(100*ms2.st_g3)
			end
		when ms.st3_act_m9_final is not null and ms.st3_act_m9_final > 0 then 
			case when ms.st3_act_m9_final >= 0.1 then 10
			when ms.st3_act_m9_final = 0 then 99
			else ceiling(100*ms.st3_act_m9_final)
			end
		else 99
		end as st3_decile
	   ,cc.TU3_COPY_APP as st3_tu_score -- TU ST3 model

	   ,d.bank_acct_cd -- flag for checking/savings; currently declining values of '4' (no checking or savings account)
	   --,d.EMP_STATUS_CD 

	   -- Various logic to determine which PV model should generate predictions, and various other important segmentations
	   ,case when d.all9220 between 0 and 9990 and d.CAMPAIGN_ID >= 90 then 'PBK'
			when d.all9220 between 0 and 9990 and m.prescreenFICO >= 700 and m.prescreenFICO <= 950 then 'PBK700'
			when d.all9220 between 0 and 9990 then 'PBK'
			else 'CNP' 
		end as model_scorecard

		,case when SUBSTRING(d.AFF_SESSION_ID,5,3) <> 'CRK' then 
			case when d.all9220 between 0 and 9990 and ((m.prescreenFICO >= 700 and m.prescreenFICO <= 950) or (d.CAMPAIGN_ID >= 90 and d.FICO_SCORE >= 700 and d.FICO_SCORE <= 950)) then 'PBK700'
				when d.all9220 between 0 and 9990 and ((m.prescreenFICO >= 250 and m.prescreenFICO <= 600) or (d.CAMPAIGN_ID >= 90 and d.FICO_SCORE >= 250 and d.FICO_SCORE <= 600) or m.prescreenFICO = 9002 or (d.CAMPAIGN_ID >= 90 and d.FICO_SCORE = 9002)) then 'EA'
				when d.all9220 between 0 and 9990 then 'PBK'
				when ((m.prescreenFICO >= 680 and m.prescreenFICO <= 950) or (d.CAMPAIGN_ID >= 90 and d.FICO_SCORE >= 680 and d.FICO_SCORE <= 950)) then 'LP'
				when ms.fsf_seg = 'UNP' or p.AT_ACCOUNT_OPEN_DT >= '2018-07-01' then 'UNP' -- don't have coverage for UNP field after June '18, but mostly stopped WNP by then
				when ms.fsf_seg = 'WNP' then 'WNP'
				else 'error' 
				end
			when SUBSTRING(d.AFF_SESSION_ID,5,3) = 'CRK' then 
				case when d.all9220 between 0 and 9990 and ((d.TU_Vantage_3 >= 250 and d.TU_Vantage_3 <= 600) or (d.TU_Vantage_3 is null and d.VANTAGE_SCORE >= 250 and d.VANTAGE_SCORE <= 600) or (d.TU_Vantage_3 is null and d.VANTAGE_SCORE is null and d.FICO_SCORE >= 250 and d.FICO_SCORE <= 600)) then 'EA'
					when d.all9220 between 0 and 9990 then 'PBK'
					when (d.TU_Vantage_3 >= 680 and d.TU_Vantage_3 <= 950) or (d.TU_Vantage_3 is null and d.VANTAGE_SCORE >= 680 and d.VANTAGE_SCORE <= 950) or (d.TU_Vantage_3 is null and d.VANTAGE_SCORE is null and d.FICO_SCORE >= 680 and d.FICO_SCORE <= 950) then 'LP'
					when p.AT_ACCOUNT_OPEN_DT > '2018-08-27' then 'UNP'
					when p.AT_ACCOUNT_OPEN_DT >= '2017-12-12' and p.AT_ACCOUNT_OPEN_DT <= '2018-08-27' then case when (o.node>0 and o.node<=4) then 'WNP' when (o.node>4 and o.node<=9) then 'UNP' else 'error' end
					-- for apps before 12/12/17, using Experian based UNP/WNP tree (otherwise can't distinguish)
					when ms.fsf_seg = 'UNP' then 'UNP'
					when ms.fsf_seg = 'WNP' then 'WNP'
					else 'error'
				end
			else 'error'
		end as pop_seg
		,case when d.all9220 between 0 and 9990 and d.FICO_SCORE >= 700 and d.FICO_SCORE <= 950 then 'PBK700'
			when d.all9220 between 0 and 9990 then 'PBK'
			when d.FICO_SCORE >= 680 and d.FICO_SCORE <= 950 then 'LP'
			when ms.fsf_seg = 'UNP' then 'UNP'
			when ms.fsf_seg = 'WNP' then 'WNP'
			else 'error' 
		end as pop_seg_app
		,d.all9220 as time_since_BK
		,m.MonthsSinceLastBKODischarged as time_since_BK_mkt
		,case when d.CAMPAIGN_ID >= 90 then 'Digital'
			when d.CAMPAIGN_ID = 89 then 'F&F'
			else 'DM'
		end as channel
		,case when d.CAMPAIGN_ID < 89 then 'DM'
			when d.CAMPAIGN_ID = 89 then 'F&F'
			when SUBSTRING(d.AFF_SESSION_ID,5,3) = 'CRK' then 'Karma'
			when SUBSTRING(d.AFF_SESSION_ID,5,3) = 'EXP' then 'ERO'
			when SUBSTRING(d.AFF_SESSION_ID,5,3) = 'CRS' then 'Sesame'
			else 'Other'
		end as affiliate
	   ,d.FICO_SCORE as app_fico
	   ,m.prescreenFICO as mktg_fico

	   	   -- additional models & credit attributes for hard cut logic
	   -- Logic to pick different model for utilization cut in Karma starting July '18 where we have retro-score availability
	   ,case when SUBSTRING(d.AFF_SESSION_ID,5,3) = 'CRK' and d.PeriodID_Experian >= 20180710 then cc.pred_uti
			when u.pred_util_v2 is not null then u.pred_util_v2
			when ms2.pred_util_v2 is not null then ms2.pred_util_v2
			else 99
			end as util_score
	   ,case when u.pred_util_v2 is not null or ms2.pred_util_v2 is not null then 1 else 0 end as util_model_hit
	   ,((case when ms.PREMIER_V1_2_FIP5020 >= 999999990 then 0 else ms.PREMIER_V1_2_FIP5020 end) + 
			(case when ms.PREMIER_V1_2_REH5030 >= 999999990 then 0 else ms.PREMIER_V1_2_REH5030 end)) 
		as unsec_debt
	   ,case when ms.PREMIER_V1_2_FIP5020 is null and ms.PREMIER_V1_2_REH5030 is null then 0 else 1 end as debt_hit
	   ,case when cast(d.BCC5030 as float) > 999999990 then 0 else cast(d.BCC5030 as float) end as bc_debt
	   ,case when cast(d.ALL8220 as float) > 9990 then 0 else cast(d.ALL8220 as float) end as age_on_file

	   ,[AT_ACCOUNT_OPEN_DT] as open_dt  

	   ---------------------------------
	   -- Various performance data 
	   --------------------------------- 
	    
	   ,[AT_CREDITLIMIT]    
	   --,[AT_PURCHASE_APR_RT] as curr_apr
	   --,[AT_CURRENT_CREDITSCORE] as fico_refresh			
  
       -- Standard DQ-based metrics
	   ,([BS_AMT_CURRENTLY_PAST_DUE_151_180_DAYS]+[BS_AMT_CURRENTLY_PAST_DUE_121_150_DAYS]+[BS_AMT_CURRENTLY_PAST_DUE_91_120_DAYS]+[BS_AMT_CURRENTLY_PAST_DUE_61_90_DAYS] 
		+[BS_AMT_CURRENTLY_PAST_DUE_31_60_DAYS]) as dq30_am
		,case when ([BS_AMT_CURRENTLY_PAST_DUE_151_180_DAYS]+[BS_AMT_CURRENTLY_PAST_DUE_121_150_DAYS]+[BS_AMT_CURRENTLY_PAST_DUE_91_120_DAYS]+[BS_AMT_CURRENTLY_PAST_DUE_61_90_DAYS] 
		+[BS_AMT_CURRENTLY_PAST_DUE_31_60_DAYS]) > 0 then 1 else 0 end as dq30_ct
		,case when ([BS_AMT_CURRENTLY_PAST_DUE_151_180_DAYS]+[BS_AMT_CURRENTLY_PAST_DUE_121_150_DAYS]+[BS_AMT_CURRENTLY_PAST_DUE_91_120_DAYS]+[BS_AMT_CURRENTLY_PAST_DUE_61_90_DAYS] 
		+[BS_AMT_CURRENTLY_PAST_DUE_31_60_DAYS]) > 0 then [AT_CREDITLIMIT] else 0 end as bad_line_dq30

		-- various DQ buckets for DQ/roll-rate based risk projections
		,case when [BS_AMT_CURRENTLY_PAST_DUE_151_180_DAYS] > 0 then 1 else 0 end as dq6_ct
		,case when [BS_AMT_CURRENTLY_PAST_DUE_121_150_DAYS] > 0 then 1 else 0 end as dq5_ct
		,case when [BS_AMT_CURRENTLY_PAST_DUE_91_120_DAYS] > 0 then 1 else 0 end as dq4_ct
		,case when [BS_AMT_CURRENTLY_PAST_DUE_61_90_DAYS] > 0 then 1 else 0 end as dq3_ct
		,case when [BS_AMT_CURRENTLY_PAST_DUE_31_60_DAYS] > 0 then 1 else 0 end as dq2_ct
		,case when [BS_AMT_CURRENTLY_PAST_DUE_1_30_DAYS] > 0 then 1 else 0 end as dq1_ct

	   -- BRM predictions
	   ,ubr.pbad_pred_brm02_sun as brm_pred
	   ,case when ubr.pbad_pred_brm02_sun >= 0 and ubr.pbad_pred_brm02_sun <= 1 then 1 else 0 end as brm_valid

		
	   -- C/O related stuff
	   ,case when p.AT_MONTHSONBOOKS = chg.chg_off_mob and AccountTypeFlag <> 'Transfer' then 1 else 0 end as bad
	   ,case when p.AT_MONTHSONBOOKS = chg.chg_off_mob and AccountTypeFlag <> 'Transfer' then BS_MONTH_ENDINGBALANCE else 0 end as GUCO
	   ,case when p.AT_MONTHSONBOOKS = chg.chg_off_mob and AccountTypeFlag <> 'Transfer' then [AT_CREDITLIMIT] else 0 end as bad_line
	   ,case when p.AT_MONTHSONBOOKS = chg.chg_off_mob and AccountTypeFlag = 'Transfer' then BS_MONTH_ENDINGBALANCE else 0 end as fraud_loss
	   ,((case when p.AT_MONTHSONBOOKS = chg.chg_off_mob then BS_month_ENDINGBALANCE else 0 end) - (case when PL_CONTRAEXPENSERECOVERIES is null then 0 else PL_CONTRAEXPENSERECOVERIES end)) as NUCO
	   
		-- Other Performance Metrics
 
       ,[CD_PURCHASE_AM] as purchase_am	
	   ,[CD_INTERCHANGE_AM]  as interchange
	   ,(case when PL_EXPENSEREWARDS is null then 0 else PL_EXPENSEREWARDS end) as rewards_cost
	   ,[CD_INTEREST_AM] as interest_revenue
	   ,(case when PL_COSTOFFUNDS is null then 0 else PL_COSTOFFUNDS end) as interest_expense			
       ,[CD_TOTALREVENUE_AM] as total_revenue
	   ,icl.ICL
	   ,case when icl.ICL = 0 then 0 else cast(p.AT_CREDITLIMIT as decimal)/cast(icl.ICL as decimal) end as line_growth
	   ,case when p.AT_MONTHSONBOOKS = attr.attr_mob then 1 else 0 end as attr
	   ,[CD_NETFINANCECHARGE_AM]   
       ,CD_FEE_ANNUAL_AM as AMF
	   ,[CD_FEE_CASH_AM] as cash_fees
	   ,[CD_CASHADVANCE_REGULAR_AM] as cash_advance_amt
	   ,[CD_FEE_TOTAL_LATE_AM] as late_fees	
	   ,case when CD_PURCHASE_BALANCETRANSFER_AM = 0 then 0
			when 0.04*CD_PURCHASE_BALANCETRANSFER_AM > 5 then 0.04*CD_PURCHASE_BALANCETRANSFER_AM
			else 5
		end as bt_fees
	   ,CD_FEE_PROMO_AM 	
	   ,CD_CYCLEBALANCEADB as AOS_ME
	   ,CD_RAW_ADB as Revolving_AOS_ME
	   ,case when c.AOS >= 0 then c.AOS else CD_CYCLEBALANCEADB end as AOS
	   ,case when c.Revolving_AOS >= 0 then c.Revolving_AOS else CD_RAW_ADB end as Revolving_AOS
	   ,case when CD_PURCHASE_CT>0 or CD_TOTAL_CASHADVANCE_AM>0 or [CD_PURCHASE_BALANCETRANSFER_CT]>0 then 1 else 0 end as debit_active 
	   ,case when BS_MONTH_BEGINNINGBALANCE > 0 or BS_MONTH_ENDINGBALANCE >0 then 1 else 0 end as balance_active 
	   ,case when ST_EXTERNAL_STATUS = 'C' then 1 else 0 end as vol_closed_ind 		
	   ,case when ST_EXTERNAL_STATUS in ('Z','C') then 0 else 1 end as open_accts
	   ,1 as orig_accts
	   -- End of performance data
	   


 FROM	
	FSF_DataMart.dbo.Account_performance_MonthEnd_Consolidated as p -- primary performance table
	-- revolve rate & utilization
	left join 
	(SELECT
	AT_ACCOUNTID
	,AT_MONTHSONBOOKS
	,(CASE when (BS_CYCLE_BEGINNINGBALANCE > 0) AND ((CD_NETPAYMENTS_AM*-1) < BS_CYCLE_BEGINNINGBALANCE) THEN  CD_CYCLEBALANCEADB else 0 end) as Revolving_AOS
	,CD_CYCLEBALANCEADB as AOS
	from  
	[FSF_DataMart].[dbo].[Account_Performance_Cycleend]) c on p.AT_ACCOUNTID = c.AT_ACCOUNTID and p.AT_MONTHSONBOOKS = c.AT_MONTHSONBOOKS
	-- C/O's
	left join 
	(SELECT 
	AT_ACCOUNTID, min(AT_MONTHSONBOOKS) as chg_off_mob from FSF_DataMart.dbo.Account_performance_MonthEnd_Consolidated where ST_EXTERNAL_STATUS = 'Z'
	group by AT_ACCOUNTID
	) chg on p.AT_ACCOUNTID = chg.AT_ACCOUNTID
	-- Voluntary Attrition
	left join 
	(SELECT 
	AT_ACCOUNTID, min(AT_MONTHSONBOOKS) as attr_mob from FSF_DataMart.dbo.Account_performance_MonthEnd_Consolidated where ST_EXTERNAL_STATUS = 'C'
	group by AT_ACCOUNTID
	) attr on p.AT_ACCOUNTID = attr.AT_ACCOUNTID
	-- ICL's
	left join 
	(SELECT 
	AT_ACCOUNTID, [AT_CREDITLIMIT] as ICL from FSF_DataMart.dbo.Account_performance_MonthEnd_Consolidated where AT_MONTHSONBOOKS = 0
	) icl on p.AT_ACCOUNTID = icl.AT_ACCOUNTID
	-- Table to link offer ID to account ID
	inner join FSF_ADW.dbo.Master_Account as j
		on p.AT_ACCOUNTID=j.AccountID 
	-- Primary app info table
	left outer join experian_raw.[dbo].[RAW_APPLICATION_DECISIONS] as d
		on d.offer_ID = j.offerID and d.decision_cd = 'A'
	-- Fields needed to exclude lost/stolen accounts
	left outer join [FSF_DataMart].[dbo].[Raw_CMF_Base_Aggregated] as cmf  
		on cmf.[CHD_ACCOUNT_NUMBER] = p.AT_ACCOUNTID and cast(year(cmf.PeriodID) as char(4))+case when month(cmf.PeriodID) <10 then '0' else '' end+cast(month(cmf.PeriodID) as char(2)) = p.AT_PERIODID
			and p.ST_EXTERNAL_STATUS = cmf.CHD_EXTERNAL_STATUS 

	-- Various tables for retroscores
	left join (
	select 
	CASE WHEN LEN(u.offer_id) = 9 THEN '0' + u.offer_id ELSE u.offer_id END as offer_id
	,pred_util_v2
	from [FSF_UserDB].[dbo].[ollo_STG3_by_PRODUCT_RESP] u
	) u on d.offer_ID = u.offer_id
	left join Acxiom_Raw.dbo.FairSquare_Solicited_Uniseq m on d.offer_ID = m.OfferID
	left join Fsf_userdb.dbo.ollo_scores_master_seg ms on d.offer_ID = ms.offer_ID
	left join [FSF_UserDB].[dbo].[ollo_exp_st3_util_jul18_feb19_2] ms2 on d.offer_ID = ms2.offer_ID
	left join fsf_userdb.dbo.retrofitted_ck_4_unp o on d.offer_id = o.offer_id
	left join [FSF_UserDB].[dbo].[ccai_TU_Scores_20180710_20190311] cc on d.offer_ID = cc.offer_id
	left join fsf_userdb.dbo.ck_applications_0829_4_unp cktu on d.OFFER_ID = cktu.OFFER_ID

	-- Drayton generated consolidated table for marketing stage Gen2 LT scores; will need to update this table eventually to include later campaigns
	left join Fsf_userdb.dbo.marketing_model_scores mkt on d.offer_ID = mkt.OfferID

	-- James' BRM prediction table
	left join FSF_userdb.dbo.[JM_BRM02_from_SAGAR] ubr on d.offer_ID = ubr.offerid and p.AT_PERIODID = ubr.month_end

WHERE
	--campaigns desired
	ST_EXTERNAL_STATUS not in ('U','L')
	and 
	d.CAMPAIGN_ID <> 89 and d.CAMPAIGN_ID > 1
	and (	(cmf.chd_coff_reason_code is null) or
		(cmf.[CHD_COFF_REASON_CODE]<>'88' -- exclude third party fraud chargeoff
		and ST_EXTERNAL_STATUS not in ('U') -- exclude stolen
		and ((ST_EXTERNAL_STATUS = ('L') and cmf.[CHD_LAST_STATEMENTED_BALANCE] <> '0' ) or ST_EXTERNAL_STATUS not in ('L')) -- exclude lost with no balance
		)
	 )