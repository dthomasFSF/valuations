# -*- coding: utf-8 -*-
"""
Created on Thu Oct 25 15:42:54 2018

@author: DraytonThomas
"""
# Required packages
import pandas as pd
import numpy as np
import math
import datetime
import matplotlib.pyplot as pt
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.ticker as mtick
from matplotlib.pyplot import cm

# Support functions for model monitoring plotter tools


######################################
# Function to read in multiple official model export sheets, 
# ID DM vs. Digital & Segment, calculate driver curves, and 
# concatenate final result
##########################################
def import_drivers(excels,ttc_offset=1.05):
    drivers = []
    for k in range(len(excels)):
        d = pd.read_excel(excels[k][0],header=16)
        d['Channel'] = excels[k][1]
        d['Customer Segment'] = excels[k][2]
        drivers.append(d)
    drivers = pd.concat(drivers,axis=0)
    
    del(d)
    
    drivers['lt'] = pd.to_numeric(drivers['LT Ventile'].str[-2:])
    drivers['st'] = pd.to_numeric(drivers['ST Decile'].str[-2:])
    
    drivers = drivers.sort_values(['Channel','Customer Segment','lt','st','MOB'])
    
    # Get open/original curve
    orig = drivers[drivers['MOB']==1][['Channel','Customer Segment','lt','st','#Open']]
    orig['orig'] = orig['#Open']
    orig = orig.drop('#Open',axis=1)
    drivers = pd.merge(left=drivers,right=orig,how='left',on=['Channel','Customer Segment','lt','st'])
    
    # Calculate driver curves
    drivers['Pbad2'] = drivers['Pbad']/ttc_offset
    
    drivers['inc_co_orig'] = drivers['Pbad2']/drivers['orig']
    drivers['cum_co_orig'] = drivers.groupby(['Channel','Customer Segment','lt','st'])['inc_co_orig'].cumsum(axis=0)
    
    drivers['inc_attr_orig'] = drivers['Voluntary Attrition'] / drivers['orig']
    drivers['cum_attr_pred'] = drivers.groupby(['Channel','Customer Segment','lt','st'])['inc_attr_orig'].cumsum(axis=0)
    
    drivers['open_per_orig'] = drivers['#Open']/drivers['orig']

    drivers['pbad_pred'] = drivers['Pbad2'] / drivers['#Open']
    drivers['line_growth_pred'] = drivers['Credit Line'] / drivers['Initial Credit Line']
    drivers['clo_pred'] = drivers['Credit Line'] / drivers['#Open']
    drivers['bad_avg_line_pred_shifted'] = np.where(drivers['MOB'] <= 8 
        ,(drivers['Bad Line'] / drivers['Credit Line'])
        ,((drivers['Bad Line']/drivers['#Open'])/(drivers['Credit Line'].shift(8)/drivers['#Open'].shift(8)))
        )
    drivers['bad_avg_line_pred'] = (drivers['Bad Line'] / drivers['Credit Line'])

    drivers['util_co_pred'] = np.where(drivers['Bad Line']>0,drivers['$OS (Bad)'] / drivers['Bad Line'],1)
    drivers['pd_fees_pred'] = drivers['Past Due Fees'] / drivers['#Open']
    drivers['util_pred'] = drivers['$OS'] / drivers['Credit Line']
    drivers['rev_rt_pred'] = drivers['Revolving Balance'] / drivers['$OS']
    drivers['attr_pred'] = drivers['Voluntary Attrition'] / drivers['#Open']
    drivers['pvol_os_pct_pred'] = drivers['Purchase Volume'] / drivers['$OS']
    drivers['pvol_pred'] = drivers['Purchase Volume'] / drivers['#Open']
    drivers['bt_fees_pred'] = drivers['BT Fees'] / drivers['#Open']
    drivers['int_rev_pred'] = drivers['Interest Revenue'] / drivers['#Open']
    drivers['int_yield_pred'] = drivers['Interest Revenue'] / drivers['$OS']
    drivers['int_yieldr_pred'] = drivers['Interest Revenue'] / drivers['Revolving Balance']
    drivers['int_chg_pred'] = drivers['Interchange'] / drivers['#Open']
    drivers['amf_pred'] = drivers['AMF'] / drivers['#Open']
    drivers['nuco_pred'] = drivers['NUCO'] / drivers['$OS']
    drivers['int_exp_pred'] = drivers['Interest Expense'] / drivers['#Open']
    drivers['rwd_exp_pred'] = drivers['Rewards Cost'] / drivers['#Open']
    drivers['ops_cost_pred'] = drivers['Ops Cost'] / drivers['#Open']
    drivers['guco_pred'] = drivers['GUCO'] / drivers['$OS']
    drivers['fraud_pred'] = drivers['Fraud Cost'] / drivers['Purchase Volume']
    drivers['acq_cost_pred'] = drivers['Acquisition Cost'] / drivers['#Open']
    drivers['cash_fees_pred'] = drivers['Cash Fees']/drivers['#Open']
    drivers['cash_vol_pred'] = drivers['Cash Advance']/drivers['#Open']
    
    drivers = drivers[['Channel','Customer Segment','lt','st','MOB'
            ,'inc_co_orig'
            ,'cum_co_orig'
            ,'pbad_pred'
            ,'open_per_orig'
            ,'line_growth_pred'
            ,'clo_pred'
            ,'bad_avg_line_pred'
            ,'bad_avg_line_pred_shifted'
            ,'util_co_pred'
            ,'pd_fees_pred'
            ,'util_pred'
            ,'rev_rt_pred'
            ,'attr_pred'
            ,'cum_attr_pred'
            ,'pvol_os_pct_pred'
            ,'pvol_pred'
            ,'bt_fees_pred'
            ,'int_rev_pred'
            ,'int_yield_pred'
            ,'int_yieldr_pred'
            ,'int_chg_pred'
            ,'amf_pred'
            ,'nuco_pred'
            ,'int_exp_pred'
            ,'rwd_exp_pred'
            ,'ops_cost_pred'
            ,'guco_pred'
            ,'fraud_pred'
            ,'acq_cost_pred'
            ,'cash_fees_pred'
            ,'cash_vol_pred'
            ]]

    return drivers


#files = [
#        ['pbk_driver_file__18-10-25_09-06-04.xlsx','DM','PBK']
#        ,['pbk_karma_driver_file__18-10-25_09-12-58.xlsx','Digital','PBK']
#        ,['pbk700_driver_file__18-10-25_09-01-35.xlsx','DM','PBK700']
#        ,['cnp_driver_file__18-10-25_08-57-38.xlsx','DM','CNP']
#        ,['cnp_karma_driver_file__18-10-25_09-19-19.xlsx','Digital','CNP']
#        ]

def a2p_join(actuals,predictions,how='left',left_on=['affiliate','model_scorecard','lt2_ventile','app_st_ntile','mob']
    ,right_on = ['Channel','Customer Segment','lt','st','MOB']):
       
    outdata = pd.merge(left=actuals,right=predictions,how=how,left_on=left_on,right_on = right_on)
    outdata = outdata.sort_values(['offer_id','mob'])

# Apply appropriate weights to the metrics prior to aggregating

    outdata['open_pred'] = outdata['orig_accts']*outdata['open_per_orig']

    outdata['cum_co_pred_o'] = outdata['cum_co_orig']*outdata['orig_accts']
    outdata['pbad_pred_o'] = outdata['pbad_pred']*outdata['open_pred']
    outdata['line_growth_pred_o'] = outdata['line_growth_pred']*outdata['open_pred']
    outdata['cl_open_pred'] = outdata['clo_pred']*outdata['open_pred']
    outdata['cl_open_pred2'] = outdata['at_creditlimit']*outdata['open_pred']
    outdata['cl_open'] = outdata['at_creditlimit']*outdata['open_accts']
    outdata['icl_open'] = outdata['icl']*outdata['open_accts']
    outdata['bad_avg_line_pred_o'] = outdata['bad_avg_line_pred']*outdata['bad']*outdata['at_creditlimit']
    outdata['bad_avg_line_pred_o2'] = outdata['bad_avg_line_pred']*outdata['pbad_pred_o']*outdata['cl_open_pred']
    outdata['util_co_pred_o'] = outdata['util_co_pred']*outdata['bad_line']
    outdata['pd_fees_pred_o'] = outdata['pd_fees_pred']*outdata['open_pred']
    outdata['util_pred_o'] = outdata['util_pred']*outdata['at_creditlimit']*outdata['open_pred']
    outdata['rev_rt_pred_o'] = outdata['rev_rt_pred']*outdata['aos']*outdata['open_pred']
    outdata['aos_o'] = outdata['aos']*outdata['open_accts']
    outdata['revolving_aos_o'] = outdata['revolving_aos']*outdata['open_accts']
    outdata['attr_pred_o'] = outdata['attr_pred']*outdata['open_pred']
    outdata['cum_attr_pred_o'] = outdata['cum_attr_pred']*outdata['orig_accts']
    
    outdata['pvol_os_pct_pred_o'] = outdata['pvol_os_pct_pred']*outdata['aos_o']
    outdata['pvol_pred_o'] = outdata['pvol_pred']*outdata['open_pred']
    outdata['bt_fees_pred_o'] = outdata['bt_fees_pred']*outdata['open_pred']
    outdata['int_rev_pred_o'] = outdata['int_rev_pred']*outdata['open_pred']
    outdata['int_yield_pred_os'] = outdata['int_yield_pred']*outdata['aos']
    outdata['int_yield_pred_ros'] = outdata['int_yieldr_pred']*outdata['revolving_aos']
    outdata['int_chg_pred_o'] = outdata['int_chg_pred']*outdata['open_pred']
    outdata['amf_pred_o'] = outdata['amf_pred']*outdata['open_pred']
    outdata['nuco_pred_o'] = outdata['nuco_pred']*outdata['aos']
    outdata['int_exp_pred_o'] = outdata['int_exp_pred']*outdata['open_pred']
    outdata['rwd_exp_pred_o'] = outdata['rwd_exp_pred']*outdata['open_pred']
    outdata['ops_cost_pred_o'] = outdata['ops_cost_pred']*outdata['open_pred']
    outdata['guco_pred_o'] = outdata['guco_pred']*outdata['aos']
    outdata['fraud_pred_o'] = outdata['fraud_pred']*outdata['purchase_am']
    outdata['acq_cost_pred_o'] = outdata['acq_cost_pred']*outdata['open_pred']
    outdata['cash_fee_pred_o'] = outdata['cash_fees_pred']*(outdata['orig_accts']*outdata['open_per_orig'])
    outdata['cash_vol_pred_o'] = outdata['cash_vol_pred']*(outdata['orig_accts']*outdata['open_per_orig'])
    
    # Risk adjusted revenue calculations (per-account/MOB basis)
    outdata['rar_act_o'] = outdata['open_accts']*(outdata['interest_revenue']+outdata['interchange']+outdata['late_fees']
        + outdata['cash_fees'] + outdata['bt_fees'] - outdata['guco'] - outdata['rewards_cost'])
    outdata['rar_pred_o'] = (outdata['orig_accts']*outdata['open_per_orig'])*(outdata['int_rev_pred']+outdata['int_chg_pred']+outdata['pd_fees_pred']
        + outdata['cash_fee_pred_o'] + outdata['bt_fees_pred'] - outdata['guco_pred'] - outdata['rwd_exp_pred'])
    
    outdata = outdata.sort_values(['offer_id','mob'])
    outdata['cum_rar_act_o'] = outdata.groupby('offer_id')['rar_act_o'].cumsum(axis=0)
    outdata['cum_rar_pred_o'] = outdata.groupby('offer_id')['rar_pred_o'].cumsum(axis=0)
    
    return outdata


def aggprep(indata,grouplist,x):
    grp = grouplist[:]
    grp.append(x)
    outdata = indata.groupby(grp)[[
        'cum_co_pred_o'
        ,'pbad_pred_o'
        ,'rar_act_o'
        ,'rar_pred_o'
        ,'cum_rar_act_o'
        ,'cum_rar_pred_o'
        ,'open_pred'
        ,'line_growth_pred_o'
        ,'bad_avg_line_pred_o'
        ,'bad_avg_line_pred_o2'
        ,'util_co_pred_o'
        ,'pd_fees_pred_o'
        ,'util_pred_o'
        ,'rev_rt_pred_o'
        ,'attr_pred_o'
        ,'pvol_os_pct_pred_o'
        ,'pvol_pred_o'
        ,'bt_fees_pred_o'
        ,'int_rev_pred_o'
        ,'int_yield_pred_os'
        ,'int_yield_pred_ros'
        ,'int_chg_pred_o'
        ,'amf_pred_o'
        ,'nuco_pred_o'
        ,'int_exp_pred_o'
        ,'rwd_exp_pred_o'
        ,'ops_cost_pred_o'
        ,'guco_pred_o'
        ,'fraud_pred_o'
        ,'acq_cost_pred_o'
        ,'open_accts'
        ,'orig_accts'
        ,'at_creditlimit'
        ,'purchase_am'
        ,'interchange'
        ,'rewards_cost'
        ,'interest_revenue'
        ,'interest_expense'
        ,'total_revenue'
        ,'icl'
        ,'icl_open'
        ,'cl_open'
        ,'line_growth'
        ,'cl_open_pred'
        ,'cl_open_pred2'
        ,'bad'
        ,'cum_bad'
        ,'guco'
        ,'bad_line'
        ,'fraud_loss'
        ,'nuco'
        ,'amf'
        ,'cash_fees'
        ,'cash_fee_pred_o'
        ,'cash_advance_amt'
        ,'cash_vol_pred_o'
        ,'late_fees'
        ,'bt_fees'
        ,'aos'
        ,'aos_o'
        ,'revolving_aos'
        ,'revolving_aos_o'
        ,'attr'
        ,'cum_attr'
        ,'cum_attr_pred_o'
        ]].sum()

    # Reset index to allow referencing of group by columns
    outdata = outdata.reset_index()
    
    # Calculate curves for A2P plots - dividing by open/etc.
    outdata['rar_a'] = outdata['rar_act_o']/outdata['open_accts']
    outdata['rar_p'] = outdata['rar_pred_o']/outdata['open_pred']
    outdata['cum_rar_a'] = outdata['cum_rar_act_o']/outdata['orig_accts']
    outdata['cum_rar_p'] = outdata['cum_rar_pred_o'] /outdata['orig_accts']
    outdata['cum_co_a'] = outdata['cum_bad']/outdata['orig_accts']
    outdata['cum_co_p'] = outdata['cum_co_pred_o']/outdata['orig_accts']
    outdata['pbad_a'] = 12*outdata['bad']/outdata['open_accts']
    outdata['pbad_p'] = 12*outdata['pbad_pred_o']/outdata['open_pred']
    outdata['util_a'] = outdata['aos_o']/outdata['cl_open']
    outdata['util_p'] = outdata['util_pred_o']/outdata['cl_open_pred2']
    outdata['line_growth_a'] = outdata['cl_open']/outdata['icl_open']
    outdata['line_growth_p'] = outdata['cl_open_pred']/outdata['icl_open']
    outdata['clo_a'] = outdata['cl_open']/outdata['open_accts']
    outdata['clo_p'] = outdata['cl_open_pred']/outdata['open_pred']
    outdata['rev_rt_a'] = outdata['revolving_aos_o']/outdata['aos_o']
    outdata['rev_rt_p'] = outdata['rev_rt_pred_o']/outdata['aos_o']
    outdata['pd_fees_p'] = outdata['pd_fees_pred_o']/outdata['open_accts']
    outdata['pd_fees_a'] = outdata['late_fees']/outdata['open_accts']
    outdata['attr_a'] = outdata['attr']/outdata['open_accts']
    outdata['attr_p'] = outdata['attr_pred_o']/outdata['open_pred']
    outdata['cum_attr_a'] = outdata['cum_attr']/outdata['orig_accts']
    outdata['cum_attr_p'] = outdata['cum_attr_pred_o']/outdata['orig_accts']
    outdata['pvol_os_a'] = outdata['purchase_am']/outdata['aos_o']
    outdata['pvol_os_p'] = outdata['pvol_os_pct_pred_o']/outdata['aos_o']
    outdata['pvol_a'] = outdata['purchase_am']/outdata['open_accts']
    outdata['pvol_p'] = outdata['pvol_pred_o']/outdata['open_pred']
    outdata['bt_fees_a'] = outdata['bt_fees']/outdata['open_accts']
    outdata['bt_fees_p'] = outdata['bt_fees_pred_o']/outdata['open_pred']
    outdata['int_rev_a'] = outdata['interest_revenue']/outdata['open_accts']
    outdata['int_rev_p'] = outdata['int_rev_pred_o']/outdata['open_pred']
    outdata['int_yield_a'] = 12*outdata['interest_revenue']/outdata['aos']
    outdata['int_yield_p'] = 12*outdata['int_yield_pred_os']/outdata['aos']
    outdata['int_yieldr_a'] = 12*outdata['interest_revenue']/outdata['revolving_aos']
    outdata['int_yieldr_p'] = 12*outdata['int_yield_pred_ros']/outdata['revolving_aos']
    outdata['amf_a'] = outdata['amf']/outdata['open_accts']
    outdata['amf_p'] = outdata['amf_pred_o']/outdata['open_pred']
    outdata['guco_a'] = 12*outdata['guco']/outdata['aos']
    outdata['guco_p'] = 12*outdata['guco_pred_o']/outdata['aos']
    outdata['nuco_a'] = 12*outdata['nuco']/outdata['aos']
    outdata['nuco_p'] = 12*outdata['nuco_pred_o']/outdata['aos']
    outdata['bad_avg_line_a'] = (outdata['bad_line']/outdata['bad'])/(outdata['cl_open']/outdata['open_accts'])
    outdata['bad_avg_line_p'] = (outdata['bad_avg_line_pred_o']/outdata['bad'])/(outdata['cl_open_pred']/outdata['open_pred'])
    outdata['bad_avg_line_p_raw'] = (outdata['bad_avg_line_pred_o2']/outdata['pbad_pred_o'])/(outdata['cl_open_pred']/outdata['open_pred'])
    outdata['util_co_a'] = outdata['guco']/outdata['bad_line']
    outdata['util_co_p'] = np.where(outdata['bad_line']>0,outdata['util_co_pred_o']/outdata['bad_line'],1)
    outdata['b2g_a'] = (outdata['guco']/outdata['bad']) / (outdata['aos_o']/outdata['open_accts'])
    outdata['b2g_p'] = (outdata['guco_pred_o']/outdata['pbad_pred_o']) / (outdata['util_pred_o']/outdata['open_pred'])
    outdata['intr_chg_a'] = outdata['interchange']/outdata['open_accts']
    outdata['intr_chg_p'] = outdata['int_chg_pred_o']/outdata['open_pred']
    outdata['rwd_exp_a'] = outdata['rewards_cost']/outdata['open_accts']
    outdata['rwd_exp_p'] = outdata['rwd_exp_pred_o']/outdata['open_pred']
    outdata['fraud_a'] = outdata['fraud_loss']/outdata['purchase_am']
    outdata['fraud_p'] = outdata['fraud_pred_o']/outdata['purchase_am']
    outdata['cash_fee_a'] = outdata['cash_fees']/outdata['open_accts']
    outdata['cash_fee_p'] = outdata['cash_fee_pred_o']/outdata['open_pred']
    outdata['cash_vol_a'] = outdata['cash_advance_amt']/outdata['open_accts']
    outdata['cash_vol_p'] = outdata['cash_vol_pred_o']/outdata['open_pred']

    
    # Create column to define segment ID by concatenating group by list    
    outdata['seg_id'] = outdata.iloc[:,0:len(grouplist)].apply(lambda x: ' '.join(x.astype(str)),axis=1)
    
    mob_cut = indata.groupby(grouplist)['max_mob'].min()
    mob_cut = mob_cut.reset_index()
    
    outdata = pd.merge(outdata,mob_cut,how='left',on=grouplist)
    
    return outdata


def a2p(indata,outfile,grouplist,x,variables,max_horizon = 96):
    
    agg_data = aggprep(indata,grouplist,x)
    
    a2p_pdf(indata=agg_data,outfile=outfile,segment='seg_id',variables=variables,series=x,max_horizon=max_horizon)
    
    return

def a2p_footers(indata,outfile,grouplist,x,variables,max_horizon = 96):
    
    agg_data = aggprep(indata,grouplist,x)
    
    # Calculate dynamic strings for footnotes, based on data filters in tmp data set
    dates = 'Apps Received: ' + str(indata['open_dt'].min().date()) + ' through ' + str(indata['open_dt'].max().date())  + ' || '
    product = 'Products: ' + str(indata['product_nm'].unique()) + ' || ' 
    popn = 'Populations: ' + str(indata['pop_seg'].unique()) + ' || ' 
    lt = 'LT Ventiles: ' + str(indata['lt2_ventile'].min()) + '-' + str(indata['lt2_ventile'].max())  + ' || '
    mob = 'MOBs: ' + str(indata['mob'].min()) + '-' + str(indata['mob'].max()) + ' || '
    channel = 'Channels: ' + str(indata['channel'].unique()) + ' || '
    promo = 'Promos: ' + str(indata['fdr_purch_promo_in'].unique()) + ' || '
    amfs = 'AMFs: ' + str(indata['fdr_annual_fee_in'].unique()) + ' || ' 
    footnote = ' || ' + dates + channel + popn + product + promo + amfs + lt + mob 
    
    a2p_pdf_footers(indata=agg_data,outfile=outfile,segment='seg_id',variables=variables,series=x,max_horizon=max_horizon,footer=footnote)
    
    return

def a2p_vintage(indata,outfile,vintage,grouplist,x,variables,max_horizon = 96):
    
    grouplist2 = grouplist[:]
    grouplist2.append(vintage)
    agg_data = aggprep(indata,grouplist2,x)
    
    #Fix seg_id values
    agg_data['seg_id'] = agg_data.iloc[:,0:len(grouplist)].apply(lambda x: ' '.join(x.astype(str)),axis=1)
    
    # Calculate dynamic strings for footnotes, based on data filters in tmp data set
    dates = 'Apps Received: ' + str(indata['open_dt'].min().date()) + ' through ' + str(indata['open_dt'].max().date())  + ' || '
    product = 'Products: ' + str(indata['product_nm'].unique()) + ' || ' 
    popn = 'Populations: ' + str(indata['pop_seg'].unique()) + ' || ' 
    lt = 'LT Ventiles: ' + str(indata['lt2_ventile'].min()) + '-' + str(indata['lt2_ventile'].max())  + ' || '
    mob = 'MOBs: ' + str(indata['mob'].min()) + '-' + str(indata['mob'].max()) + ' || '
    channel = 'Channels: ' + str(indata['channel'].unique()) + ' || '
    promo = 'Promos: ' + str(indata['fdr_purch_promo_in'].unique()) + ' || '
    amfs = 'AMFs: ' + str(indata['fdr_annual_fee_in'].unique()) + ' || ' 
    footnote = ' || ' + dates + channel + popn + product + promo + amfs + lt + mob 
        
    a2p_pdf_vintage(indata=agg_data,outfile=outfile,vintage=vintage,segment='seg_id',variables=variables,series=x,max_horizon=max_horizon,footer=footnote)
    
    return

##############################################################

#  Define standard cuts / products / etc

##############################################################

def policy_product(indata,grid,karma_retro):
    
    outdata = indata
    
    ###############################################
    # Join in other needed files
    ###############################################

    # Join in util grid logic
    outdata = pd.merge(left=outdata,right=grid,how='left',left_on='tgt_lt',right_on = 'lt')
    
    # Join in Karma TU LT ventiles
    outdata = pd.merge(left=outdata,right=karma_retro,how='left',left_on='offer_id',right_on = 'offer_id')
    outdata['tu_lt_y'].fillna(99,inplace=True)
    outdata['tu_lt_y'] = outdata['tu_lt_y'].astype('int8')
    outdata['tu_lt_cmb'] = np.where(
                outdata['tu_lt_y'] < 99
                ,outdata['tu_lt_y']
                ,outdata['tu_lt_x']
                )

    ###################################################
    # Define current rollout products
    ###################################################

    outdata['rollout_product'] = np.where(
        (((outdata['affiliate']=='DM')&(outdata['pop_seg']=='PBK')&(outdata['product_nm']=='OLLO PLATINUM')&(outdata['fdr_annual_fee_in']=='A')&(outdata['fdr_purch_promo_in']=='A')&(outdata['fdr_bt_promo_apr_in']=='A'))
        | ((outdata['affiliate']=='DM')&(outdata['pop_seg']=='UNP')&(outdata['product_nm']=='OLLO PLATINUM')&(outdata['fdr_annual_fee_in']=='A')&(outdata['fdr_purch_promo_in']=='D')&(outdata['fdr_bt_promo_apr_in']=='D'))
        | ((outdata['affiliate']=='DM')&(outdata['pop_seg']=='WNP')&(outdata['product_nm']=='OLLO PLATINUM')&(outdata['fdr_annual_fee_in']=='A')&(outdata['fdr_purch_promo_in']=='D')&(outdata['fdr_bt_promo_apr_in']=='D'))
        | ((outdata['affiliate']=='DM')&(outdata['pop_seg']=='PBK700')&(outdata['product_nm']=='OLLO PLATINUM')&(outdata['fdr_annual_fee_in']=='A')&(outdata['fdr_purch_promo_in']=='D')&(outdata['fdr_bt_promo_apr_in']=='D')&(outdata['appmonth']>=201707)))
        | ((outdata['affiliate']=='Karma')&(outdata['product_nm']=='OLLO PLATINUM')&(outdata['fdr_annual_fee_in']=='A')&(outdata['fdr_purch_promo_in']=='A')&(outdata['fdr_bt_promo_apr_in']=='A'))
        | ((outdata['affiliate']=='ERO')&(outdata['product_nm']=='OLLO REWARDS')&(outdata['fdr_annual_fee_in']=='A')&(outdata['fdr_purch_promo_in']=='A')&(outdata['fdr_bt_promo_apr_in']=='A'))
        ,1,0)
       
    # Define LT groups
    lt_conds = [(outdata['lt2_ventile']>=1) & (outdata['lt2_ventile']<=3)
        ,(outdata['lt2_ventile']>=4) & (outdata['lt2_ventile']<=5)
        ,(outdata['lt2_ventile']>=6) & (outdata['lt2_ventile']<=8)
        ,(outdata['lt2_ventile']>=9) & (outdata['lt2_ventile']<=10)
        ,(outdata['lt2_ventile']>=11) & (outdata['lt2_ventile']<=13)
        ,(outdata['lt2_ventile']>=14) & (outdata['lt2_ventile']<=20)
        ]
    
    lt_groups = ['LT 01-03','LT 04-05','LT 06-08','LT 09-10','LT 11-13','LT 14-20']
    
    outdata['lt_group'] = np.select(lt_conds,lt_groups,default='LT - Undefined')
    
    lt_conds2 = [(outdata['lt2_ventile']>=1) & (outdata['lt2_ventile']<=8)
        ,(outdata['lt2_ventile']>=9) & (outdata['lt2_ventile']<=13)
        ,(outdata['lt2_ventile']>=14) & (outdata['lt2_ventile']<=20)
        ]
    
    lt_groups2 = ['Core (LT 1-8)','Marginal (LT 9-13)','Swap-Out (LT 14+)']
    
    outdata['lt_group2'] = np.select(lt_conds2,lt_groups2,default='LT - Undefined')
    
    ###################################################
    # Define key credit cuts (e.g. recent pullbacks)
    ###################################################
    
    # Utilization model
    
    outdata['util_ventile'] = 5*(outdata['util_score']//0.05)
    outdata['util_ventile'] = outdata['util_ventile'].apply(lambda x: min(100,max(0,x)))
    
    outdata['util_decile'] = 10*(outdata['util_score']//0.1)
    outdata['util_decile'] = outdata['util_decile'].apply(lambda x: min(100,max(0,x)))
    
    # LT cut & Other Binary rules
    
    outdata['lt_cut'] = np.where(
            ((outdata['affiliate']=='DM')&(outdata['lt2_ventile']>13)&(outdata['lt2_ventile']!=99)) |
            ((outdata['affiliate']=='ERO')&(outdata['app_lt_ntile']>7)&(outdata['app_lt_ntile']!=99)) |
            ((outdata['affiliate']=='Karma')&(outdata['pop_seg'].isin(['PBK','PBK700']))&(outdata['tu_lt_cmb']>10)&(outdata['tu_lt_cmb']!=99)) |
            ((outdata['affiliate']=='Karma')&(outdata['pop_seg'].isin(['UNP','WNP','CNP']))&(outdata['tu_lt_cmb']>8)&(outdata['tu_lt_cmb']!=99)) |
            ((outdata['affiliate']=='Sesame')&(outdata['lt2_ventile']>18)&(outdata['lt2_ventile']!=99)) 
            ,1,0)
    
    outdata['debt_cut'] = np.where(
            ((outdata['affiliate']=='DM')&(outdata['debt_hit']==1) &
             ((outdata['unsec_debt'] > 17000) | ((outdata['unsec_debt'] > 12000) & (outdata['lt2_ventile'] >= 9)))) |
            ((outdata['affiliate']=='Karma')&
             (((outdata['unsec_debt'] > 15000)&(outdata['debt_hit']==1)) | (outdata['bc_debt'] > 12000))) |
            ((outdata['affiliate']=='ERO')&(outdata['debt_hit']==1) &
             ((outdata['unsec_debt'] > 12000) | ((outdata['unsec_debt'] > 10000) & (outdata['app_lt_ntile'] >= 6)))) 
           ,1,0)
    
    outdata['bk_time_cut'] = np.where(
            ((outdata['affiliate'].isin(['ERO','Karma','Sesame'])) & (outdata['time_since_bk']>=84) & (outdata['time_since_bk'] < 9990)) |
            ((outdata['affiliate']=='DM') & (outdata['time_since_bk']>=87) & (outdata['time_since_bk'] < 9990)) |
            ((outdata['affiliate']=='DM') & (outdata['time_since_bk']>=76) & (outdata['time_since_bk'] < 9990) & (outdata['lt2_ventile'] >= 9))
            ,1,0) # adding 3 months to account for marketing-app timing
    
    outdata['util_cut'] = np.where(
            (outdata['util_model_hit'] ==1) & (outdata['util_score'] < 0.2)
            ,1,0)
    
    outdata['grid_cut'] = np.where(
            ((outdata['affiliate']=='DM') & (outdata['pop_seg']=='PBK') & (outdata['util_ventile'] < outdata['grid_a']) & (outdata['util_model_hit'] ==1)) |
            ((outdata['affiliate']=='DM') & (outdata['pop_seg']=='PBK700') & (outdata['util_ventile'] < outdata['grid_b']) & (outdata['util_model_hit'] ==1)) |
            ((outdata['affiliate']=='DM') & (outdata['pop_seg']=='UNP') & (outdata['util_ventile'] < outdata['grid_b']) & (outdata['util_model_hit'] ==1)) |
            ((outdata['affiliate']=='DM') & (outdata['pop_seg']=='WNP') & (outdata['util_ventile'] < outdata['grid_d']) & (outdata['util_model_hit'] ==1)) |
            ((outdata['affiliate']=='DM') & (outdata['pop_seg']=='error') & (outdata['util_ventile'] < outdata['grid_b']) & (outdata['util_model_hit'] ==1)) |
            ((outdata['affiliate']=='DM') & (outdata['pop_seg']=='LP') & (outdata['util_ventile'] < outdata['grid_c']) & (outdata['util_model_hit'] ==1)) |
            ((outdata['affiliate']=='Karma') & (outdata['util_cut'] == 1)) 
            ,1,0)
    
    outdata['thin_cut'] = np.where(
            ((outdata['affiliate']=='ERO') & (outdata['pop_seg'].isin(['UNP','WNP','CNP'])) & (outdata['age_on_file'] < 61)) |
            (outdata['age_on_file'] < 24)
            ,1,0)
    
    outdata['bank_acct_cut'] = np.where(outdata['bank_acct_cd']==4,1,0)
    
    outdata['any_cut'] = outdata[['lt_cut','debt_cut','bk_time_cut','grid_cut','thin_cut','bank_acct_cut']].apply(np.max,axis=1)
    
    # Other Stuff
    outdata['BK_bkt'] = np.where((outdata['time_since_bk']<=9990)&(outdata['time_since_bk']>=0)
        ,outdata['time_since_bk'].apply(lambda x: 12*math.floor(x/12)),-99)
       
    # Define ICL groups
    icl_conds = [(outdata['icl']<=300)
        ,(outdata['icl']>300) & (outdata['icl']<=500)
        ,(outdata['icl']>500) & (outdata['icl']<=1000)
        ,(outdata['icl']>1000) & (outdata['icl']<=1500)
        ,(outdata['icl']>1500) & (outdata['icl']<=2000)
        ,(outdata['icl']>2000) & (outdata['icl']<=2500)
        ,(outdata['icl']>2500) & (outdata['icl']<=3000)
        ,(outdata['icl']>3000)
        ]
    
    icl_groups = [300,500,1000,1500,2000,2500,3000,3500]
    
    outdata['icl_group'] = np.select(icl_conds,icl_groups,default='missing')
    
    outdata['open_dt']=pd.to_datetime(outdata['open_dt'])
    
    
    
    return outdata



##############################################################

#  Call-able QMM variable sets

##############################################################

triple = [
 ['rar_p','rar_a','Risk Adjusted Revenue per Orig']
,['cum_rar_p','cum_rar_a', 'Cum. Risk Adjusted Revenue per Orig']
,['cum_co_p','cum_co_a', 'Cum. CO per Orig']       
,['pbad_p','pbad_a', 'Pbad per Open (Annualized)']
,['util_p','util_a','Utilization per Open']
,['rev_rt_p','rev_rt_a', 'Revolve Rate per Open']
,['line_growth_p','line_growth_a','Line Growth per Open']
,['guco_p','guco_a','GUCO% per Open (Annualized)']
,['b2g_p','b2g_a','Bad-to-Good Balance Ratio']
,['bad_avg_line_p_raw','bad_avg_line_a','Bad Line / Avg Line per Open']
,['util_co_p','util_co_a','Util @ CO']
,['cum_attr_p','cum_attr_a','Cum. Attrition per Orig']
,['attr_p','attr_a','Attrition per Open']
,['pvol_os_p','pvol_os_a','PVol as % of $OS']
,['intr_chg_p','intr_chg_a','Interchange']
,['fraud_p','fraud_a','Fraud as % of PVol']
,['cash_fee_p','cash_fee_a', 'Cash Fees per Open']
,['bt_fees_p','bt_fees_a','BT Fees per Open']
,['pd_fees_p','pd_fees_a','Late Fees per Open']
,['int_rev_p','int_rev_a','Interest Revenue per Open']
,['int_yield_p','int_yield_a','Interest Yield (Annualized)']
,['int_yieldr_p','int_yieldr_a','Interest Yield of Revolving Bal (Annualized)']
]

qmm_views = [
 ['cum_rar_p','cum_rar_a', 'Cum. Risk Adjusted Revenue per Orig']
,['cum_co_p','cum_co_a', 'Cum. CO per Orig']       
,['pbad_p','pbad_a', 'Pbad per Open (Annualized)']
,['util_p','util_a','Utilization per Open']
,['rev_rt_p','rev_rt_a', 'Revolve Rate per Open']
,['line_growth_p','line_growth_a','Line Growth per Open']
,['clo_p','clo_a','Credit Line per Open']
,['bad_avg_line_p_raw','bad_avg_line_a','Bad Line / Avg Line per Open']
,['util_co_p','util_co_a','Util @ CO']
,['guco_p','guco_a','GUCO% per Open (Annualized)']
,['b2g_p','b2g_a','Bad-to-Good Balance Ratio']
,['cum_attr_p','cum_attr_a','Cum. Attrition per Orig']
,['attr_p','attr_a','Attrition per Open']
,['pd_fees_p','pd_fees_a','Late Fees per Open']
,['pvol_p','pvol_a','Purchase Volume per Open'] 
,['pvol_os_p','pvol_os_a','PVol as % of $OS']
,['cash_fee_p','cash_fee_a', 'Cash Fees per Open']
]
    

    