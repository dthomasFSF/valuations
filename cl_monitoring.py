# -*- coding: utf-8 -*-
"""
Created on Wed May 29 15:34:48 2019

@author: DraytonThomas
"""

# Set Directory
%cd "C:\Users\DraytonThomas\OneDrive - Fair Square Financial LLC\Valuation Models\Infrastructure"

# Call other files:
runfile('C:/Users/DraytonThomas/OneDrive - Fair Square Financial LLC/Valuation Models/Infrastructure/actuals_pull_function.py', wdir='C:/Users/DraytonThomas/OneDrive - Fair Square Financial LLC/Valuation Models/Infrastructure')
runfile('C:/Users/DraytonThomas/OneDrive - Fair Square Financial LLC/Valuation Models/Infrastructure/curve_plotter.py', wdir='C:/Users/DraytonThomas/OneDrive - Fair Square Financial LLC/Valuation Models/Infrastructure')
runfile('C:/Users/DraytonThomas/OneDrive - Fair Square Financial LLC/Valuation Models/Infrastructure/support_functions.py', wdir='C:/Users/DraytonThomas/OneDrive - Fair Square Financial LLC/Valuation Models/Infrastructure')

###############################################
# Read in Models & monitoring variables
###############################################

files = [
        ['drivers_pbk_ck__19-04-18_14-02-20.xlsx','Karma','PBK']
        ,['drivers_unp_ck__19-04-18_14-07-02.xlsx','Karma','CNP']
        ,['drivers_pbk_ero__19-04-18_14-14-04.xlsx','ERO','PBK']
        ,['drivers_unp_ero__19-04-18_14-18-38.xlsx','ERO','CNP']
        ]

drivers = import_drivers(excels=files, ttc_offset=1.05)

lg = drivers[drivers['MOB'].isin([1,5,7,11,14,18,26])][['Channel','Customer Segment','lt','st','MOB','clo_pred']]

tmp = lg[['Channel','Customer Segment','lt','st']]
tmp = tmp.drop_duplicates(['Channel','Customer Segment','lt','st'])


for k in lg['MOB'].unique():
    tmpk = lg[lg['MOB']==k][['Channel','Customer Segment','lt','st','clo_pred']]
    tmpk['clo_mob'+str(k)] = tmpk['clo_pred']
    tmpk = tmpk.drop('clo_pred',axis=1)
    
    tmp = pd.merge(left=tmp,right=tmpk,how='left',on=['Channel','Customer Segment','lt','st'])

# next step is to calculate ICL & CLIP metrics


tmp['icl'] = tmp['clo_mob1']
tmp['clip6'] = tmp['clo_mob11'] - tmp['clo_mob5']
tmp['clip6i'] = tmp['clo_mob7'] - tmp['clo_mob5']
tmp['clip12'] = tmp['clo_mob18'] - tmp['clo_mob11']
tmp['clip12i'] = tmp['clo_mob14'] - tmp['clo_mob11']
tmp['clip_later'] = tmp['clo_mob26'] - tmp['clo_mob18']

cl_pred = tmp[['Channel','Customer Segment','lt','st','icl','clip6','clip6i','clip12','clip12i','clip_later']]


# try on a2p file

a2p_dig = pd.read_csv('a2p_dig_2019-05-17.csv')

line_mos = [1,5,7,11,14,18,26]

lg = a2p_dig[a2p_dig['MOB'].isin([1,5,7,11,14,18,26])][['offer_id','affiliate','appqtr','appmonth','lt_group2','tu_lt_cmb','max_mob'
        ,'pop_seg','product_nm','rollout_product','any_cut','mob','clo_pred','cl_open_pred','open_pred','cl_open','open_accts']]

tmp = lg[['offer_id','affiliate','appqtr','appmonth','lt_group2','tu_lt_cmb','max_mob'
        ,'pop_seg','product_nm','rollout_product','any_cut']]
tmp = tmp.drop_duplicates(['offer_id','affiliate','appqtr','appmonth','lt_group2','tu_lt_cmb','max_mob'
        ,'pop_seg','product_nm','rollout_product','any_cut'])


for k in lg['mob'].unique():
    tmpk = lg[lg['mob']==k][['offer_id','clo_pred','cl_open','open_accts']]
    tmpk['clo_mob_p'+str(k)] = tmpk['clo_pred']
    tmpk['clo_mob_a'+str(k)] = tmpk['cl_open']
    tmpk['open_mob'+str(k)] = tmpk['open_accts']
    tmpk = tmpk.drop(['clo_pred','cl_open','open_accts'],axis=1)
    
    tmp = pd.merge(left=tmp,right=tmpk,how='left',on='offer_id')

# next step is to calculate ICL & CLIP metrics
# need to figure out handling of nulls for calculations interactions w/ aggregation

tmp['icl_p'] = tmp['clo_mob_p1']
tmp['icl_a'] = tmp['clo_mob_a1']
tmp['clip6_p'] = tmp['clo_mob_p11'] - tmp['clo_mob_p5']
tmp['clip6_a'] = tmp['clo_mob_a11'] - tmp['clo_mob_a5']
tmp['clip6i_p'] = tmp['clo_mob_p7'] - tmp['clo_mob_p5']
tmp['clip6i_a'] = tmp['clo_mob_a7'] - tmp['clo_mob_a5']

tmp['clip12_p'] = tmp['clo_mob_p18'] - tmp['clo_mob_p11']
tmp['clip12_a'] = tmp['clo_mob_a18'] - tmp['clo_mob_a11']

tmp['clip12i_p'] = tmp['clo_mob_p14'] - tmp['clo_mob_p11']
tmp['clip12i_a'] = tmp['clo_mob_a14'] - tmp['clo_mob_a11']

tmp['clip_later_p'] = tmp['clo_mob_p26'] - tmp['clo_mob_p18']
tmp['clip_later_a'] = tmp['clo_mob_a26'] - tmp['clo_mob_a18']


cl_pred = tmp[['affiliate','appqtr','appmonth','lt_group2','tu_lt_cmb','max_mob'
               ,'pop_seg','product_nm','rollout_product','any_cut'
                  ,'icl_a','clip6_a','clip6i_a','clip12_a','clip12i_a','clip_later_a'
                  ,'icl_p','clip6_p','clip6i_p','clip12_p','clip12i_p','clip_later_p']]

