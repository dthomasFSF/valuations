// Creator: Drayton Thomas
// Description: create daily view of transactions at account level
// Last Updated: 3/20/20


create or replace table USER_DB.DTHOMAS.daily_transactions as
SELECT 

p.DATA_DT AS DATE_PROCESSED
,(p.DATA_DT - dayofweek(p.DATA_DT) + 1) as week
,weekofyear(p.DATA_DT) as week_of_yr
,(dayofweek(p.DATA_DT) + 1) as day_of_week

,year(p.DATA_DT)*100 + month(p.DATA_DT) as cal_month
,p.RDT_CHD_ACCOUNT_NUMBER_SCRAMBLED as accountID

,sum(
  (case when p.RDT_Transaction_Code = '271' and p.RDT_Batch_Type not in ('4','5') then cast(cast(p.rdt_transaction_amount as float) as numeric(15,4)) else 0 end)
 - (case when p.RDT_Transaction_Code = '272' then cast(cast(p.rdt_transaction_amount as float) as numeric(15,4)) else 0 end)   
) as NET_PAYMENTS

,sum(case when p.RDT_Transaction_Code = '271' and p.RDT_Batch_Type not in ('4','5') then cast(cast(p.rdt_transaction_amount as float) as numeric(15,4)) else 0 end) as PAYMENT_TOTAL
,sum(case when p.RDT_Transaction_Code = '271' and p.RDT_Batch_Type not in ('4','5') then cast(cast(p.RDT_COFF_PRIN_PAID as float) as numeric(15,4)) else 0 end) as PAYMENT_PRIN
,sum(case when p.RDT_Transaction_Code = '271' and p.RDT_Batch_Type not in ('4','5') then cast(cast(p.RDT_COFF_INT_PAID as float) as numeric(15,4)) else 0 end) as PAYMENT_NON_PRIN

,sum(case when p.RDT_Transaction_Code = '253' and p.RDT_Batch_Type not in ('4','5') and trim(p.rdt_mrch_name) not like '%ANNUAL CHARGE%' and p.RDT_MRCH_NAME NOT IN  ('BALANCE TRANSFER','CASH ADVANCE', '', ' ') then cast(cast(p.rdt_transaction_amount as float) as numeric(15,4)) else 0 end) as Purchase
,sum(case when p.RDT_Transaction_Code = '272' then cast(cast(p.rdt_transaction_amount as float) as numeric(15,4)) else 0 end) as paymentReversals
,sum(case when p.RDT_Transaction_Code = '254' and p.RDT_Batch_Type not in ('4','5') then cast(cast(p.rdt_transaction_amount as float) as numeric(15,4)) else 0 end) as CashAdvances
,sum(case when p.RDT_Transaction_Code = '255' and p.RDT_Batch_Type not in ('4','5') then cast(cast(p.rdt_transaction_amount as float) as numeric(15,4)) else 0 end) as ALL_Returns
,sum(case when p.RDT_Transaction_Code = '255' and p.RDT_Batch_Type not in ('4','5') then (case when cast(cast(p.rdt_transaction_amount as float) as numeric(15,4))>0 then 1 else 0 end) else 0 end) as Num_All_Returns
,sum(case when p.RDT_Transaction_Code = '253' and p.RDT_Batch_Type not in ('4','5') and trim(p.RDT_MRCH_NAME) = 'BALANCE TRANSFER' then cast(cast(p.rdt_transaction_amount as float) as numeric(15,4)) else 0 end) as BalanceTransfer
,sum(case when p.RDT_Transaction_Code = '253' and p.RDT_Batch_Type not in ('4','5') and trim(p.rdt_mrch_name) not like '%ANNUAL CHARGE%' and p.RDT_MRCH_NAME NOT IN  ('BALANCE TRANSFER','CASH ADVANCE', '', ' ') then (case when cast(cast(p.rdt_transaction_amount as float) as numeric(15,4))>0 then 1 else 0 end) else 0 end) as num_of_purchase
,sum(case when p.RDT_Transaction_Code = '271' and p.RDT_Batch_Type not in ('4','5') then (case when cast(cast(p.rdt_transaction_amount as float) as numeric(15,4))>0 then 1 else 0 end) else 0 end) as num_of_payment
,sum(case when p.RDT_Transaction_Code = '253' and p.RDT_Batch_Type not in ('4','5') and trim(p.RDT_MRCH_NAME) = 'BALANCE TRANSFER' then (case when cast(cast(p.rdt_transaction_amount as float) as numeric(15,4))>0 then 1 else 0 end) else 0 end) as Num_BalanceTransfer


FROM FIRSTDATA.NONPII.POSTED_MON_BASE p

group by 1,2,3,4,5,6
;
    

//select * from USER_DB.DTHOMAS.daily_transactions limit 100

select
dt.cal_month
,week
,date_processed
,day_of_week
,week_of_yr
,dt.portfolio_desc
,pt.payment_behavior
,sum(payment_total) as payments_total
,sum(payment_prin) as payments_prin
,sum(payment_non_prin) as payments_non_prin
from USER_DB.DTHOMAS.daily_transactions dt
    left join USER_DB.DTHOMAS.pay_types pt on dt.accountid = pt.CHD_ACCOUNT_NUMBER_SCRAMBLED and dt.cal_month=pt.cal_month
group by 1,2,3,4,5,6,7



-- bom bal
create or replace table USER_DB.DTHOMAS.bom_bals as
    select 
    a.CHD_ACCOUNT_NUMBER_SCRAMBLED
     ,year(a.data_dt)*100 + month(a.data_dt) as cal_month
     ,a.data_dt 
     ,a.CHD_EXTERNAL_STATUS
     ,a.chd_status_reason_code
     ,case when a.chd_portfolio_no = '00000' then 'D - Unknown'
        when a.chd_portfolio_no = '01111' then 'B - Warehouse'
        when a.chd_portfolio_no = '02222' then 'C - FSF Equity'
        when a.chd_portfolio_no = '03333' then 'A - Trust'
        else 'E - Missing'
      end as portfolio_desc
    ,(CHD_CTD_AMT_DEBITS+CHDAP_CURR_CTD_MRCH_PRIN+CHDAP_DISPUTED_AMOUNT+CHDAP_MRCH_MUF+CHDAP_MUF_DISPUTE+
    CHDAP_OPEN_CYC_FLAP_PRIN+CHDAP_OPEN_CYC_LOAN_PRIN+CHDAP_OPEN_CYC_MRCH_BINT+CHDAP_OPEN_CYC_MRCH_NBINT+
    CHDAP_TOTL_MMB_AM)  AS FSF_TOT_MRCH_PRIN_BAL_AMT

    ,(CHDAP_CASH_MUF+CHDAP_CURR_CTD_CASH_PRIN+CHDAP_OPEN_CYC_CASH_PRIN) AS FSF_TOT_CASH_PRIN_BAL_AMT

    ,(CHD_OVERPAYMENT_AMT*-1) AS FSF_OVERPAYMENT_AMT
    ,(FSF_TOT_MRCH_PRIN_BAL_AMT+FSF_TOT_CASH_PRIN_BAL_AMT+FSF_OVERPAYMENT_AMT) AS FSF_TOT_PRIN_BAL_AMT
    ,CASE WHEN CHD_EXTERNAL_STATUS NOT IN ('Z') THEN FSF_OVERPAYMENT_AMT ELSE '0' END AS FSF_NONCO_OVERPAYMENT_AMT
    ,CASE WHEN CHD_EXTERNAL_STATUS NOT IN ('Z') THEN FSF_TOT_PRIN_BAL_AMT ELSE '0' END AS FSF_NONCO_TOT_PRIN_BAL_AMT
    ,CASE WHEN CHD_EXTERNAL_STATUS NOT IN ('Z') THEN FSF_TOT_MRCH_PRIN_BAL_AMT ELSE '0' END AS FSF_NONCO_TOT_MRCH_PRIN_BAL_AMT
    ,CASE WHEN CHD_EXTERNAL_STATUS NOT IN ('Z') THEN FSF_TOT_CASH_PRIN_BAL_AMT ELSE '0' END AS FSF_NONCO_TOT_CASH_PRIN_BAL_AMT
    ,(CHD_CAINT_CTD_AMT+CHD_CAINT_LS_AMT+CHD_MRCHINT_AMT+
        CHDAP_MUF_CASH_INTSC+CHDAP_MUF_MRCH_INTSC+CHDAP_OPEN_CYC_INTSC-CHD_CRDINT_AMT)  AS FSF_TOT_FIN_CHARGE_AMT
    ,CASE WHEN CHD_EXTERNAL_STATUS NOT IN ('Z') THEN FSF_TOT_FIN_CHARGE_AMT ELSE '0' END AS FSF_NONCO_TOT_FIN_CHARGE_AMT
    ,(CHD_CTD_AMT_ITEM_CHG+CHD_CTD_AMT_LATE_CHG+CHD_CTD_AMT_MISC_CHGS+CHD_CTD_ANNUAL_CHARGE+CHD_CTD_OVERLIMIT_CHG+
        CHD_CTD_SALE_ITEM_CHGS+CHD_MUF_CTD_ANNL_CHRG_AM+CHD_MUF_CTD_ITEM_CHRG_AM+CHD_MUF_CTD_LATE_CHRG_AM+CHD_MUF_CTD_MSCL_CHRG_AM+
        CHD_MUF_CTD_OVRL_CHRG_AM+CHD_MUF_CTD_SRCH_AM+CHDAP_MISC_CHGS+CHDAP_MISCELLANEOUS+CHDAP_MUF_UNPD_ANNL_CHRG_AM+
        CHDAP_MUF_UNPD_CRDT_LIFE_AM+CHDAP_MUF_UNPD_ITEM_CHRG_AM+CHDAP_MUF_UNPD_LATE_CHRG_AM+CHDAP_MUF_UNPD_MSCL_CHRG_AM+
        CHDAP_MUF_UNPD_OVRL_CHRG_AM+CHDAP_MUF_UNPD_SRCH_AM+CHDAP_SRCHG_AM+CHDAP_UNPD_ANNL_CHRG_AM+CHDAP_UNPD_CASH_ITEM_AM+
        CHDAP_UNPD_CRDLF_PRMM_AM+CHDAP_UNPD_LATE_CHRG_AM+CHDAP_UNPD_OVRL_CHRG_AM+CHDAP_UNPD_SALE_ITEM_AM+
        CHD_CTD_SRCHG_AM_1+CHD_CTD_SRCHG_AM_2+CHD_CTD_SRCHG_AM_3+CHD_CTD_SRCHG_AM_4+CHD_CTD_SRCHG_AM_5) AS FSF_TOT_FEE_AMT
    ,CASE WHEN CHD_EXTERNAL_STATUS NOT IN ('Z') THEN FSF_TOT_FEE_AMT ELSE '0' END AS FSF_NONCO_TOT_FEE_AMT
    ,(FSF_TOT_FIN_CHARGE_AMT+FSF_TOT_FEE_AMT) AS FSF_TOT_INTR_AMT
    ,CASE WHEN CHD_EXTERNAL_STATUS NOT IN ('Z') THEN FSF_TOT_INTR_AMT ELSE '0' END AS FSF_NONCO_TOT_INTR_AMT

    from FIRSTDATA.NONPII.CARDHOLDER_MASTER a
          inner join 
              (select CHD_ACCOUNT_NUMBER_SCRAMBLED, year(data_dt)*100 + month(data_dt) as cal_month, min(data_dt) as data_dt from FIRSTDATA.NONPII.CARDHOLDER_MASTER group by 1,2) b 
              on a.CHD_ACCOUNT_NUMBER_SCRAMBLED = b.CHD_ACCOUNT_NUMBER_SCRAMBLED and a.data_dt = b.data_dt
    order by 1,2,3
;

select 
cal_month
,sum(FSF_TOT_PRIN_BAL_AMT) as prin_bal_bom
,sum(FSF_NONCO_TOT_PRIN_BAL_AMT) as prin_bal_bom_nonco
from USER_DB.DTHOMAS.bom_bals
group by 1
order by 1

select 
p.at_periodid
,bom.cal_month
,sum(bom.FSF_TOT_PRIN_BAL_AMT) as prin_bal_bom
,sum(bom.FSF_NONCO_TOT_PRIN_BAL_AMT) as prin_bal_bom_nonco
from firstdata.nonpii.account_performance_cycle_end p
left join USER_DB.DTHOMAS.bom_bals bom
        on p.at_accountid = bom.CHD_ACCOUNT_NUMBER_SCRAMBLED 
        and p.at_periodid = bom.cal_month 
//        and p.st_external_status = bom.CHD_EXTERNAL_STATUS 	
group by 1,2
order by 1,2

select * from (
select 
bom.CHD_ACCOUNT_NUMBER_SCRAMBLED 
,bom.cal_month 
,count(*) as records
,sum(bom.FSF_NONCO_TOT_PRIN_BAL_AMT)
from USER_DB.DTHOMAS.bom_bals bom
group by 1,2
)
where records > 1

select * from FIRSTDATA.NONPII.CARDHOLDER_MASTER limit 10

select * from FIRSTDATA.NONPII.POSTED_MON_BASE limit 10


select
CHD_ACCOUNT_NUMBER_SCRAMBLED
,data_dt
,CHD_DATE_LAST_PAYMENT
,TO_DATE('20'||RIGHT(NULLIF(LTRIM(CHD_DATE_LAST_PAYMENT,'0'),''),6),'YYYYMMDD')
,CHD_DATE_LAST_STMT
,TO_DATE('20'||RIGHT(NULLIF(LTRIM(CHD_DATE_LAST_STMT,'0'),''),6),'YYYYMMDD')
,CHD_PYMN_DUE_DT
,TO_DATE('20'||RIGHT(NULLIF(LTRIM(CHD_PYMN_DUE_DT,'0'),''),6),'YYYYMMDD')

from FIRSTDATA.NONPII.CARDHOLDER_MASTER
where CHD_DATE_LAST_PAYMENT is not null
order by 1,2
limit 1000

select
data_dt
,year(data_dt)*100+month(data_dt) as numdate
,count(CHD_ACCOUNT_NUMBER_SCRAMBLED) as accounts
,sum(case when CHD_DATE_LAST_PAYMENT is null then 0 else 1 end) as last_pay_dt_hit
,sum(case when CHD_DATE_LAST_STMT is null then 0 else 1 end) as last_stmt_dt_hit
,sum(case when CHD_PYMN_DUE_DT is null then 0 else 1 end) as due_pay_dt_hit
from FIRSTDATA.NONPII.CARDHOLDER_MASTER
group by 1,2
order by 2,1


select
    chd.CHD_ACCOUNT_NUMBER_SCRAMBLED
    ,chd.data_dt
    ,year(chd.data_dt)*100+month(chd.data_dt) as cal_month
    ,(chd.data_dt - dayofweek(chd.data_dt)+1) as week
    ,weekofyear(chd.data_dt) as week_of_yr
    ,dayofweek(chd.data_dt)+1 as day_of_week
    
    ,chd.CHD_DATE_LAST_PAYMENT
    ,chd.CHD_DATE_LAST_STMT
    ,chd.CHD_PYMN_DUE_DT

    ,sum(dt.payment_total) as payments_total
    ,sum(dt.payment_prin) as payments_prin
    ,sum(dt.payment_non_prin) as payments_non_prin
    
from FIRSTDATA.NONPII.CARDHOLDER_MASTER chd
    left join USER_DB.DTHOMAS.daily_transactions dt
        on chd.CHD_ACCOUNT_NUMBER_SCRAMBLED = dt.accountID
        and chd.data_dt = dt.date_processed
where chd.CHD_DATE_LAST_PAYMENT is not null
group by 1,2,3,4,5,6,7,8,9
order by 1,2
limit 1000




