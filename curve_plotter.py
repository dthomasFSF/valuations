# -*- coding: utf-8 -*-
"""
Created on Mon Oct 15 16:05:40 2018

@author: DraytonThomas
"""

#%cd "C:\Users\DraytonThomas\OneDrive - Fair Square Financial LLC\Valuation Models\Infrastructure"

import matplotlib.pyplot as pt
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.ticker as mtick
from matplotlib.pyplot import cm
import numpy as np

   
#############################################################
# Curve Plotting Functionality
#############################################################

def curve_plotter(indata,segment,variables,series,max_horizon=96):
    for k in indata[segment].unique():
        tmp = indata[(indata[segment]==k) & (indata[series]<=max_horizon)]
        for c in variables:
          pt.plot(tmp[series],tmp[c],'k-')
          pt.legend(['Expected'],loc = 'best')
          pt.title(k + ': ' + c)
          pt.xlabel(series)
          pt.show()
    return

def curve_plotter_2l(indata,segment,variables,series,max_horizon=96):
    for k in indata[segment].unique():
        tmp = indata[(indata[segment]==k) & (indata[series]<=max_horizon)]
        for c in variables:
          pt.plot(tmp[series],tmp[c[0]],'k--')
          pt.plot(tmp[series],tmp[c[1]],'r-')
          pt.legend([c[0],c[1]],loc = 'best')
          pt.title(k)
          pt.xlabel(series)
          pt.show()
    return

def curve_pdf(indata,outfile,segment,variables,series,max_horizon=96):
    with PdfPages(outfile+'.pdf') as pdf:
        for k in indata[segment].unique():
            tmp = indata[(indata[segment]==k) & (indata[series]<=max_horizon)]
            for c in variables:
              pt.figure()
              pt.plot(tmp[series],tmp[c],'k-')
              pt.legend(['Expected'],loc = 'best')
              pt.title(k + ': ' + c)
              pt.xlabel(series)
              pdf.savefig()
              pt.close()
    return 

def curve_pdf_2l(indata,outfile,segment,variables,series,max_horizon=96):
    with PdfPages(outfile+'.pdf') as pdf:
        for k in indata[segment].unique():
            tmp = indata[(indata[segment]==k) & (indata[series]<=max_horizon)]
            for c in variables:
              pt.figure()
              pt.plot(tmp[series],tmp[c[0]],'k--')
              pt.plot(tmp[series],tmp[c[1]],'r-')
              pt.legend([c[0],c[1]],loc = 'best')
              pt.title(k)
              pt.xlabel(series)
              pdf.savefig()
              pt.close()
    return


def a2p_pdf(indata,outfile,segment,variables,series,max_horizon=96):
    with PdfPages(outfile+'.pdf') as pdf:
        for k in indata[segment].unique():
            tmp = indata[(indata[segment]==k) & (indata[series]<=max_horizon) & (indata['mob']<=indata['max_mob'])]
            for c in variables:
              pt.figure()
              pt.plot(tmp[series],tmp[c[0]],'k--',label = 'Model Expected')
              pt.plot(tmp[series],tmp[c[1]],'r-',label = 'Actual')
              pt.legend(loc='best')
              pt.title(k + ' - ' + c[2])
              pt.xlabel(series)
              pdf.savefig()
              pt.close()
    return

def a2p_pdf_footers(indata,outfile,segment,variables,series,max_horizon=96,footer=''):
    # initialize PDF report
    with PdfPages(outfile+'.pdf') as pdf:
        
        # Express global filters in report at the top
        pt.figure()
        pt.figtext(0.5,0.5,'Global Report Filters: ' + footer,horizontalalignment='center',fontsize='large',wrap=True)
        pdf.savefig()
        pt.close()

        #iterate through segments
        for k in indata[segment].unique():
            tmp = indata[(indata[segment]==k) & (indata[series]<=max_horizon) & (indata['mob']<=indata['max_mob'])]

            # show sample size at top of segment
            pt.figure()
            pt.plot(tmp[series],tmp['open_accts'],'b-',label = '# Open Accounts (Sample Size)')
            pt.legend(loc='best')
            pt.title(k)
            pt.xlabel(series)
            pdf.savefig()
            pt.close()
            
            # iterate through variable list to make plots
            for c in variables:
              pt.figure()
              pt.plot(tmp[series],tmp[c[0]],'k--',label = 'Model Expected')
              pt.plot(tmp[series],tmp[c[1]],'r-',label = 'Actual')
              pt.legend(loc='best')
              pt.title(k + ' - ' + c[2])
              pt.xlabel(series)
              #pt.figtext(0.01,0.95,footer,horizontalalignment='left',fontsize='xx-small',wrap=True)
              pdf.savefig()
              pt.close()
    return

# create view for quarters
def a2p_pdf_vintage(indata,outfile,vintage,segment,variables,series,max_horizon=96,footer=''):
    # initialize PDF report
    with PdfPages(outfile+'.pdf') as pdf:
        
        # Express global filters in report at the top
        pt.figure()
        pt.figtext(0.5,0.5,'Global Report Filters: ' + footer,horizontalalignment='center',fontsize='large',wrap=True)
        pdf.savefig()
        pt.close()

        #iterate through segments
        for k in indata[segment].unique():
            tmp = indata[(indata[segment]==k) & (indata[series]<=max_horizon) & (indata['mob']<=indata['max_mob'])]
            color = cm.rainbow(np.linspace(0,1,len(tmp[vintage].unique())))
            # show sample size at top of segment
            pt.figure()
            for v in range(len(tmp[vintage].unique())):
                  tmp_v = tmp[tmp[vintage]==tmp[vintage].unique()[v]]
                  pt.plot(tmp_v[series],tmp_v['open_accts'],label = tmp[vintage].unique()[v],c=color[v])
            #pt.plot(tmp[series],tmp['open_accts'],label = '# Open Accounts (Sample Size)')
            pt.legend(loc='best')
            pt.title(k + ': Open Accounts by Vintage')
            pt.xlabel(series)
            pdf.savefig()
            pt.close()
            
            # iterate through variable list to make plots
            for c in variables:
              tmp['a2p'] = (tmp[c[1]] / tmp[c[0]])
              pt.figure()
              pt.plot(tmp[series].unique(),np.linspace(1,1,len(tmp[series].unique())),'k--',label='Reference')
              for v in range(len(tmp[vintage].unique())):
                  tmp_v = tmp[tmp[vintage]==tmp[vintage].unique()[v]]
                  pt.plot(tmp_v[series],tmp_v['a2p'],label = tmp[vintage].unique()[v],c=color[v])
              pt.legend(loc='best')
              pt.title(k + ' - A2P Ratio: ' + c[2])
              #pt.ylim(0.5,1.5)
              pt.xlabel(series)
              pdf.savefig()
              pt.close()
    return