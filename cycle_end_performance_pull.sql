// Creator: Drayton Thomas
// Description: pull standard performance views, but from cycle end performance table
// Last Updated: 3/23/20

// Caveats: code runs without error, but I have not actually looked at any views from this, so there could still be some issue
//          currently, the underlying performance table does not update daily

set portfolio_asOf_dt = '2020-02-19';


create or replace table USER_DB.DTHOMAS.cycle_end_perf as 

select 

	   d.OFFER_ID
	   ,p.AT_MONTHSONBOOKS as MOB	
       
       ,p.AT_ACCOUNTID 
	   ,p.AT_PERIODID as cal_month

       ,d.Campaign_ID 
	   ,year(p.AT_ACCOUNT_OPEN_DT)*100 + MONTH(p.AT_ACCOUNT_OPEN_DT) as appmonth
	   ,to_char(YEAR(p.AT_ACCOUNT_OPEN_DT)) || 'Q' || ltrim(to_char(CEIL(month(p.AT_ACCOUNT_OPEN_DT)/3))) as appqtr
       ,p.AT_ACCOUNT_OPEN_DT as open_dt 
       
       ,p.AT_CYCLEEND_DT as cycle_end_dt -- can report out verticals by this field
       
       -- Channel, Affiliate & Pop_Seg
       
        ,case 
			when d.CAMPAIGN_ID = '89' then 'F&F'
            when d.solicitation_chan_in = 1 then 'DM'
			else 'Digital'
		end as channel
        
		,case when d.CAMPAIGN_ID = '89' then 'F&F'
            when d.solicitation_chan_in = 1 then 'DM'
			when SUBSTRING(d.affiliate_sess_id,5,3) = 'CRK' then 'Karma'
			when SUBSTRING(d.affiliate_sess_id,5,3) = 'EXP' then 'ERO'
			when SUBSTRING(d.affiliate_sess_id,5,3) = 'CRS' then 'Sesame'
//            when d.CAMPAIGN_ID in (97,98,96) and d.affiliate_sess_id = ' ' then 'Sesame'
//            when d.affiliate_sess_id = '' and ((d.CAMPAIGN_ID = 80 and d.campaign_sub_cd in (0,3)) OR (d.campaign_ID = 81 and d.campaign_sub_cd in (3,4)))
			else 'Other'
		end as affiliate

		,case when SUBSTRING(d.affiliate_sess_id,5,3) <> 'CRK' then 
			case when d.all9220 between 0 and 9990 and ((try_cast(m.prescreenFICO as decimal) > 700 and try_cast(m.prescreenFICO as decimal) <= 950) or (d.solicitation_chan_in <> 1 and try_cast(d.FICO_SCORE as decimal) > 700 and try_cast(d.FICO_SCORE as decimal) <= 950)) then 'FA700'
                  when d.all9220 between 0 and 9990 and ((try_cast(m.prescreenFICO as decimal) >= 250 and try_cast(m.prescreenFICO as decimal) <= 600) or (d.solicitation_chan_in <> 1 and try_cast(d.FICO_SCORE as decimal) >= 250 and try_cast(d.FICO_SCORE as decimal) <= 600) 
                                                         or (try_cast(m.prescreenFICO as decimal) <= 700 and m.PopulationCellDescription like '%BF%')) then 'EA'
                  when d.all9220 between 0 and 9990 and (try_cast(m.prescreenFICO as decimal) = 9002 or (d.solicitation_chan_in <> 1 and try_cast(d.FICO_SCORE as decimal) = 9002)) then 'EA9002'
                  when d.all9220 between 0 and 9990 then 'FA'
                  when ((try_cast(m.prescreenFICO as decimal) > 680 and try_cast(m.prescreenFICO as decimal) <= 950) or (d.solicitation_chan_in <> 1 and try_cast(d.FICO_SCORE as decimal) > 680 and try_cast(d.FICO_SCORE as decimal) <= 950)) then 'Prime'
                  when ((try_cast(m.prescreenFICO as decimal) >= 250 and try_cast(m.prescreenFICO as decimal) <= 600) or (d.solicitation_chan_in <> 1 and try_cast(d.FICO_SCORE as decimal) >= 250 and try_cast(d.FICO_SCORE as decimal) <= 600)) then 
                    case when (ms.NEW_EXP_UNP_FLAG = 1 or (ms.NEW_EXP_UNP_FLAG is null and i.cust_seg = 'SJ')) then 'SJ L&G'
                        when (ms.NEW_EXP_UNP_FLAG = 0 or (ms.NEW_EXP_UNP_FLAG is null and i.cust_seg = 'NonSJ')) then 'NonSJ L&G'
                        else 'ERROR'
                    end
                  when (ms.NEW_EXP_UNP_FLAG = 1 or (ms.NEW_EXP_UNP_FLAG is null and i.cust_seg = 'SJ')) and jay.fsf_old_seg = 'UNP' then 'SJII'
                  when (ms.NEW_EXP_UNP_FLAG = 1 or (ms.NEW_EXP_UNP_FLAG is null and i.cust_seg = 'SJ')) and jay.fsf_old_seg = 'WNP' then 'SJSI'
                  when (ms.NEW_EXP_UNP_FLAG = 0 or (ms.NEW_EXP_UNP_FLAG is null and i.cust_seg = 'NonSJ')) then 'NonSJ'
                  else 'ERROR' 
				end
			when SUBSTRING(d.affiliate_sess_id,5,3) = 'CRK' then 
				case when d.all9220 between 0 and 9990 and ((try_cast(d.VANTAGE_SCORE as decimal) >= 250 and try_cast(d.VANTAGE_SCORE as decimal) <= 600) or (try_cast(d.VANTAGE_SCORE as decimal) is null and try_cast(d.FICO_SCORE as decimal) >= 250 and try_cast(d.FICO_SCORE as decimal) <= 600)) then 'EA'
					when d.all9220 between 0 and 9990 and ((try_cast(d.VANTAGE_SCORE as decimal) > 700 and try_cast(d.VANTAGE_SCORE as decimal) <= 950) or (try_cast(d.VANTAGE_SCORE as decimal) is null and try_cast(d.FICO_SCORE as decimal) > 700 and try_cast(d.FICO_SCORE as decimal) <= 950)) then 'FA700'
                    when d.all9220 between 0 and 9990 then 'FA'
					when (try_cast(d.VANTAGE_SCORE as decimal) > 680 and try_cast(d.VANTAGE_SCORE as decimal) <= 950) or (try_cast(d.VANTAGE_SCORE as decimal) is null and try_cast(d.FICO_SCORE as decimal) > 680 and try_cast(d.FICO_SCORE as decimal) <= 950) then 'Prime'
                    when (try_cast(d.VANTAGE_SCORE as decimal) >= 250 and try_cast(d.VANTAGE_SCORE as decimal) <= 600) or (try_cast(d.VANTAGE_SCORE as decimal) is null and try_cast(d.FICO_SCORE as decimal) >= 250 and try_cast(d.FICO_SCORE as decimal) <= 600) then 
                      case when (ms.NEW_EXP_UNP_FLAG = 1 or (ms.NEW_EXP_UNP_FLAG is null and i.cust_seg = 'SJ')) then 'SJ L&G'
                        when (ms.NEW_EXP_UNP_FLAG = 0 or (ms.NEW_EXP_UNP_FLAG is null and i.cust_seg = 'NonSJ')) then 'NonSJ L&G'
                        else 'ERROR'
                      end                          
                    when (ms.NEW_EXP_UNP_FLAG = 1 or (ms.NEW_EXP_UNP_FLAG is null and i.cust_seg = 'SJ')) and jay.fsf_old_seg = 'UNP' then 'SJII'
                    when (ms.NEW_EXP_UNP_FLAG = 1 or (ms.NEW_EXP_UNP_FLAG is null and i.cust_seg = 'SJ')) and jay.fsf_old_seg = 'WNP' then 'SJSI'
                    when (ms.NEW_EXP_UNP_FLAG = 0 or (ms.NEW_EXP_UNP_FLAG is null and i.cust_seg = 'NonSJ')) then 'NonSJ'
				    else 'ERROR' 
				end
			else 'ERROR'
		end as pop_seg

	   --stuff from application decisions table
	   ,d.product_nm -- product: Platinum/Rewards/Optimum
	   ,d.FDR_PURCH_PROMO_IN -- purchase promo flag; 'A' is no promo
	   ,d.FDR_BT_PROMO_APR_IN -- BT promo flag; 'A' is no promo 
	   ,d.FDR_ANNUAL_FEE_IN -- annual fee flag; 'A' is no fee
	   ,d.RISK_SCORE_1 as App_LT_Ntile -- Gen 1 LT model (hard cut in Experian Right Offer)
	   ,d.RISK_SCORE_2 as App_ST_Ntile -- Gen 1 ST model (secondary grounding dimension for PV models)
       ,case when d.FDR_BT_PROMO_APR_IN = 'A' then 0
            when d.FDR_BT_PROMO_APR_IN = 'C' then 7
            when d.FDR_BT_PROMO_APR_IN = 'D' then 9
            when d.FDR_BT_PROMO_APR_IN = 'F' then 12
            when d.FDR_BT_PROMO_APR_IN = 'H' then 15
            else -99
           end as bt_promo_length
       ,case when bt_promo_length > 0 then
            case when bt_promo_length + 6 >= mob then (bt_promo_length - mob) else -6 end
            else -99
           end as promo_time_left

	   -- Gen 2 LT model: combining production & retro-scores to improve coverage
       ,case   
          when d.Cust_Mod_3_Fin_Scr_Dec in ('000', '448', '00') or d.Cust_Mod_3_Fin_Scr like '%y%' or d.Cust_Mod_3_Fin_Scr = 'N000000000000000000' or d.Cust_Mod_3_Fin_Scr = 'Y000000000000000000' or d.Cust_Mod_3_Fin_Scr is null then 
              case 
              when jay.gen2_1_lt > 0 then
              case when jay.gen2_1_lt < 0.19 then ceil(100*jay.gen2_1_lt) else 20 end
              else 99
              end
          when try_cast(d.Cust_Mod_3_Fin_Scr as float) > 20 then 20
          when try_cast(d.Cust_Mod_3_Fin_Scr as float) = 0 then 99
          else ceil(try_cast(d.Cust_Mod_3_Fin_Scr as float))
        end as lt2_ventile

// LT3 Model
     ,CASE WHEN try_cast(d.cust_mod_6_fin_scr as decimal) > 0 and try_cast(d.cust_mod_6_fin_scr as decimal) <= 100 and appmonth >= 201912 then 
            CASE 
                WHEN try_cast(d.cust_mod_6_fin_scr as decimal) BETWEEN 0 AND 5 THEN 1
                WHEN try_cast(d.cust_mod_6_fin_scr as decimal) BETWEEN 5 AND 6 THEN 2
                WHEN try_cast(d.cust_mod_6_fin_scr as decimal) BETWEEN 6 AND 8 THEN 3
                WHEN try_cast(d.cust_mod_6_fin_scr as decimal) BETWEEN 8 AND 9 THEN 4
                WHEN try_cast(d.cust_mod_6_fin_scr as decimal) BETWEEN 9 AND 10 THEN 5
                WHEN try_cast(d.cust_mod_6_fin_scr as decimal) BETWEEN 10 AND 13 THEN 6
                WHEN try_cast(d.cust_mod_6_fin_scr as decimal) BETWEEN 13 AND 15 THEN 7
                WHEN try_cast(d.cust_mod_6_fin_scr as decimal) BETWEEN 15 AND 16 THEN 8
                WHEN try_cast(d.cust_mod_6_fin_scr as decimal) BETWEEN 16 AND 18 THEN 9
                WHEN try_cast(d.cust_mod_6_fin_scr as decimal) BETWEEN 18 AND  19 THEN 10
                WHEN try_cast(d.cust_mod_6_fin_scr as decimal) BETWEEN  19 AND  20 THEN 11
                WHEN try_cast(d.cust_mod_6_fin_scr as decimal) BETWEEN  20 AND  21 THEN 12
                WHEN try_cast(d.cust_mod_6_fin_scr as decimal) BETWEEN  21 AND  22 THEN 13
                WHEN try_cast(d.cust_mod_6_fin_scr as decimal) BETWEEN  22 AND  23 THEN 14
                WHEN try_cast(d.cust_mod_6_fin_scr as decimal) BETWEEN  23 AND  24 THEN 15
                WHEN try_cast(d.cust_mod_6_fin_scr as decimal) BETWEEN  24 AND  25 THEN 16
                WHEN try_cast(d.cust_mod_6_fin_scr as decimal) BETWEEN  25 AND  26 THEN 17
                WHEN try_cast(d.cust_mod_6_fin_scr as decimal) BETWEEN  26 AND  28 THEN 18
                WHEN try_cast(d.cust_mod_6_fin_scr as decimal) BETWEEN  28 AND  30 THEN 19
                WHEN try_cast(d.cust_mod_6_fin_scr as decimal) BETWEEN  30 AND 100 THEN 20
                               END
        ELSE CASE
                WHEN jay.gen3_lt BETWEEN 0.00 AND 0.05 THEN 1
                WHEN jay.gen3_lt BETWEEN 0.05 AND 0.06 THEN 2
                WHEN jay.gen3_lt BETWEEN 0.06 AND 0.08 THEN 3
                WHEN jay.gen3_lt BETWEEN 0.08 AND 0.09 THEN 4
                WHEN jay.gen3_lt BETWEEN 0.09 AND 0.10 THEN 5
                WHEN jay.gen3_lt BETWEEN 0.10 AND 0.13 THEN 6
                WHEN jay.gen3_lt BETWEEN 0.13 AND 0.15 THEN 7
                WHEN jay.gen3_lt BETWEEN 0.015 AND 0.16 THEN 8
                WHEN jay.gen3_lt BETWEEN 0.16 AND 0.18 THEN 9
                WHEN jay.gen3_lt BETWEEN 0.18 AND 0.19 THEN 10
                WHEN jay.gen3_lt BETWEEN 0.19 AND 0.20 THEN 11
                WHEN jay.gen3_lt BETWEEN 0.20 AND 0.21 THEN 12
                WHEN jay.gen3_lt BETWEEN 0.21 AND 0.22 THEN 13
                WHEN jay.gen3_lt BETWEEN 0.22 AND 0.23 THEN 14
                WHEN jay.gen3_lt BETWEEN 0.23 AND 0.24 THEN 15
                WHEN jay.gen3_lt BETWEEN 0.24 AND 0.25 THEN 16
                WHEN jay.gen3_lt BETWEEN 0.25 AND 0.26 THEN 17
                WHEN jay.gen3_lt BETWEEN 0.26 AND 0.28 THEN 18
                WHEN jay.gen3_lt BETWEEN 0.28 AND 0.30 THEN 19
                WHEN jay.gen3_lt BETWEEN 0.30 AND 1.00 THEN 20
                ELSE 99 END 
         END AS LT3_band               


		-- ST3 model: have to combine production & two separate retro-score tables
    ,case when d.cust_mod_5_fin_scr = 'N000000000000000000' or d.cust_mod_5_fin_scr = 'Y000000000000000000' then 99
      when d.cust_mod_5_fin_scr is not null and try_cast(d.cust_mod_5_fin_scr as float) > 0 then 
        case when try_cast(d.cust_mod_5_fin_scr as float) >= 10 then 10
        when try_cast(d.cust_mod_5_fin_scr as float) = 0 then 99
        else ceil(try_cast(d.cust_mod_5_fin_scr as float))
        end
      when jay.gen3_st is not null and jay.gen3_st > 0 then 
        case when jay.gen3_st >= 0.1 then 10
        when jay.gen3_st = 0 then 99
        else ceil(100*jay.gen3_st)
        end
      else 99
     end as st3_decile
     
     // LT 4 Model
     
     ,jay.gen4_1_lt as lt4_score
        
      -- Utilization Model
        
      ,CASE WHEN try_cast(d.cust_mod_8_fin_scr as decimal) > 0 and try_cast(d.cust_mod_8_fin_scr as decimal) <= 100 then try_cast(d.cust_mod_8_fin_scr as decimal)/100 
          ELSE jay.exp_util 
       END as util_score

	   ,d.bank_acct_cd -- flag for checking/savings; currently declining values of '4' (no checking or savings account)

      -- Model Scorecard - which PV model should the account be tied to  
        ,case when affiliate <> 'ERO' then 
            case when trim(pop_seg) in ('FA','EA','EA9002') then 'FA'
                when trim(pop_seg) in ('FA700','Prime') then 'FA700'
                when trim(pop_seg) in ('SJII','SJ L&G') then 'SJII'
                when trim(pop_seg) in ('SJSI','NonSJ','NonSJ L&G') then 'SJSI'
                else 'ERROR'
            end
         when affiliate = 'ERO' then 
            case when trim(pop_seg) in ('FA','EA','EA9002') then 'ERO_FA'
                when trim(pop_seg) in ('FA700','Prime') then 'ERO_FA700'
                when trim(pop_seg) in ('SJII','SJ L&G') then 'ERO_SJII'
                when trim(pop_seg) in ('SJSI','NonSJ','NonSJ L&G') then 'ERO_SJSI'
                else 'ERO_ERROR'
            end
         else 'ERROR'
         end as model_scorecard
		,d.all9220 as time_since_BK
		,m.MonthsSinceLastBKODischarged as time_since_BK_mkt

	   ,d.FICO_SCORE as app_fico
	   ,m.prescreenFICO as mktg_fico

	   	   -- additional credit attributes for hard cut logic
	   ,((case when ms.PREMIER_V1_2_PIL5020 >= 999999990 then 0 else ms.PREMIER_V1_2_PIL5020 end) + 
			(case when ms.PREMIER_V1_2_REH5030 >= 999999990 then 0 else ms.PREMIER_V1_2_REH5030 end)) 
		as unsec_debt_app
       ,case when unsec_debt_app is null then i.unsec_debt else unsec_debt_app end as unsec_debt
	   ,case when unsec_debt_app is null and i.unsec_debt is null then 0 else 1 end as debt_hit
	   ,case when try_cast(d.BCC5030 as float) > 999999990 then 0 else try_cast(d.BCC5030 as float) end as bc_debt
	   ,case when try_cast(d.ALL8220 as float) > 9990 then 0 else try_cast(d.ALL8220 as float) end as age_on_file

	   ---------------------------------
	   -- Various performance data 
	   --------------------------------- 
	    
	   ,p.AT_CREDITLIMIT    
	   ,AT_PURCHASE_APR_RT as curr_apr
	   ,AT_CURRENT_CREDITSCORE as fico_refresh			
  
       -- Standard DQ-based metrics
	   ,(p.BS_AMT_CURRENTLY_PAST_DUE_151_180_DAYS + p.BS_AMT_CURRENTLY_PAST_DUE_121_150_DAYS + p.BS_AMT_CURRENTLY_PAST_DUE_91_120_DAYS + p.BS_AMT_CURRENTLY_PAST_DUE_61_90_DAYS 
		+ p.BS_AMT_CURRENTLY_PAST_DUE_31_60_DAYS) as dq30_am
		,case when (p.BS_AMT_CURRENTLY_PAST_DUE_151_180_DAYS + p.BS_AMT_CURRENTLY_PAST_DUE_121_150_DAYS + p.BS_AMT_CURRENTLY_PAST_DUE_91_120_DAYS + p.BS_AMT_CURRENTLY_PAST_DUE_61_90_DAYS 
		+ p.BS_AMT_CURRENTLY_PAST_DUE_31_60_DAYS) > 0 then 1 else 0 end as dq30_ct
		,case when (p.BS_AMT_CURRENTLY_PAST_DUE_151_180_DAYS + p.BS_AMT_CURRENTLY_PAST_DUE_121_150_DAYS + p.BS_AMT_CURRENTLY_PAST_DUE_91_120_DAYS + p.BS_AMT_CURRENTLY_PAST_DUE_61_90_DAYS 
		+ p.BS_AMT_CURRENTLY_PAST_DUE_31_60_DAYS) > 0 then AT_CREDITLIMIT else 0 end as bad_line_dq30

		-- various DQ buckets for DQ/roll-rate based risk projections
		,case when p.BS_AMT_CURRENTLY_PAST_DUE_151_180_DAYS > 0 then 1 else 0 end as dq6_ct
		,case when p.BS_AMT_CURRENTLY_PAST_DUE_121_150_DAYS > 0 then 1 else 0 end as dq5_ct
		,case when p.BS_AMT_CURRENTLY_PAST_DUE_91_120_DAYS > 0 then 1 else 0 end as dq4_ct
		,case when p.BS_AMT_CURRENTLY_PAST_DUE_61_90_DAYS > 0 then 1 else 0 end as dq3_ct
		,case when p.BS_AMT_CURRENTLY_PAST_DUE_31_60_DAYS > 0 then 1 else 0 end as dq2_ct
		,case when p.BS_AMT_CURRENTLY_PAST_DUE_1_30_DAYS > 0 then 1 else 0 end as dq1_ct


	   -- C/O related stuff
	   ,case when p.AT_MONTHSONBOOKS = chg.chg_off_mob and AccountTypeFlag <> 'Transfer' then 1 else 0 end as bad
	   ,case when p.AT_MONTHSONBOOKS = chg.chg_off_mob and AccountTypeFlag <> 'Transfer' then BS_CYCLE_ENDINGBALANCE else 0 end as GUCO
	   ,case when p.AT_MONTHSONBOOKS = chg.chg_off_mob and AccountTypeFlag <> 'Transfer' then AT_CREDITLIMIT else 0 end as bad_line
	   ,case when p.AT_MONTHSONBOOKS = chg.chg_off_mob and AccountTypeFlag = 'Transfer' then BS_CYCLE_ENDINGBALANCE else 0 end as fraud_loss
	   ,((case when p.AT_MONTHSONBOOKS = chg.chg_off_mob then BS_CYCLE_ENDINGBALANCE else 0 end) - (case when PL_CONTRAEXPENSERECOVERIES is null then 0 else PL_CONTRAEXPENSERECOVERIES end)) as NUCO
	   
		-- Other Performance Metrics
 
	   ,p.CD_INTERCHANGE_AM  as interchange
	   ,(case when p.PL_EXPENSEREWARDS is null then 0 else p.PL_EXPENSEREWARDS end) as rewards_cost
	   ,p.CD_INTEREST_AM as interest_revenue
	   ,(case when p.PL_COSTOFFUNDS is null then 0 else p.PL_COSTOFFUNDS end) as interest_expense			
       ,p.CD_TOTALREVENUE_AM as total_revenue
	   ,d.CREDIT_LINE_AMT as ICL
       ,case when d.CREDIT_LINE_AMT is null or p.AT_CREDITLIMIT is null or d.CREDIT_LINE_AMT = 0 then 0
         else p.AT_CREDITLIMIT/try_cast(d.CREDIT_LINE_AMT as decimal) 
         end as line_growth
	   ,case when p.AT_MONTHSONBOOKS = attr.attr_mob then 1 else 0 end as attr
	   ,p.CD_NETFINANCECHARGE_AM   
       ,p.CD_FEE_ANNUAL_AM as AMF
	   ,p.CD_FEE_CASH_AM as cash_fees
	   ,p.CD_CASHADVANCE_REGULAR_AM as cash_advance_amt
	   ,p.CD_FEE_TOTAL_LATE_AM as late_fees	
	   ,case when p.CD_PURCHASE_BALANCETRANSFER_AM = 0 then 0
			when 0.04*p.CD_PURCHASE_BALANCETRANSFER_AM > 5 then 0.04*p.CD_PURCHASE_BALANCETRANSFER_AM
			else 5
		end as bt_fees_est
       ,p.CD_PURCHASE_BALANCETRANSFER_AM as bt_amt
	   ,p.CD_FEE_PROMO_AM as bt_fees
	   ,case when p.ST_EXTERNAL_STATUS = 'Z' then 0 else
            p.CD_CYCLEBALANCEADB 
           end as AOS
       ,(CASE when (BS_CYCLE_BEGINNINGBALANCE > 0) AND ((CD_NETPAYMENTS_AM*-1) < BS_CYCLE_BEGINNINGBALANCE) THEN  CD_CYCLEBALANCEADB else 0 end) as Revolving_AOS
	   ,case when p.CD_PURCHASE_CT>0 or p.CD_TOTAL_CASHADVANCE_AM>0 or p.CD_PURCHASE_BALANCETRANSFER_CT>0 then 1 else 0 end as debit_active 
	   ,case when p.ST_EXTERNAL_STATUS <> 'Z' AND (p.BS_CYCLE_BEGINNINGBALANCE > 0 or p.BS_CYCLE_ENDINGBALANCE >0 or AOS > 0) then 1 else 0 end as balance_active 
	   ,case when p.ST_EXTERNAL_STATUS = 'C' then 1 else 0 end as vol_closed_ind 		
	   ,case when p.ST_EXTERNAL_STATUS in ('Z','C') then 0 else 1 end as open_accts
       ,p.ST_EXTERNAL_STATUS
	   ,1 as orig_accts
       ,p.CD_RETURNS_ADJUSTMENTS_AM 
       ,p.CD_RETURNS_ADJUSTMENTS_CT 
        ,case when p.st_external_status = 'Z' then 0 else p.BS_CYCLE_ENDINGBALANCE end as BS_CYCLE_ENDINGBALANCE
        ,p.CD_PAYMENTS_AM
        ,p.CD_PAYMENTS_CT
        ,p.CD_PURCHASE_AM
        ,p.CD_PURCHASE_CT
        ,p.CD_PURCHASE_RETURN_AM
        ,p.CD_PURCHASE_RETURN_CT
        ,p.CD_PURCHASE_BALANCETRANSFER_AM
        ,p.CD_PURCHASE_BALANCETRANSFER_CT
        ,case when p.st_external_status = 'Z' then 0 else p.CD_CYCLEBALANCEADB end as CD_CYCLEBALANCEADB 
        ,p.CD_FEE_TOTAL_LATE_AM
        
        ,(-1)*p.CD_NETPAYMENTS_AM as payments
        ,p.BS_MINIMUM_PAYMENT_DUE
        ,case when p.st_external_status = 'Z' then 0 else p.BS_CYCLE_BEGINNINGBALANCE end as BS_CYCLE_BEGINNINGBALANCE
        ,case when p.st_external_status = 'Z' then 0 else mth.BS_MONTH_BEGINNINGBALANCE end as BS_MONTH_BEGINNINGBALANCE


//        ,case 
//            when (p.BS_MINIMUM_PAYMENT_DUE = 0 or p.BS_MINIMUM_PAYMENT_DUE is null) and (-1)*p.CD_NETPAYMENTS_AM <= 0 then 'I - No Payment Due'
//            when (-1)*p.CD_NETPAYMENTS_AM >= p.BS_CYCLE_BEGINNINGBALANCE then 'H - Full Pay'
//            when (-1)*p.CD_NETPAYMENTS_AM < 0 then 'AA - Neg Pay' -- compare to min pay
//            when (-1)*p.CD_NETPAYMENTS_AM = 0 then 'A - No Pay' -- compare to min pay
//            when (-1)*p.CD_NETPAYMENTS_AM < p.BS_MINIMUM_PAYMENT_DUE then 'B - <Min Pay' -- compare to min pay
//            when (-1)*p.CD_NETPAYMENTS_AM = p.BS_MINIMUM_PAYMENT_DUE then 'C - Min Pay'  -- compare to min pay
//            when (-1)*p.CD_NETPAYMENTS_AM < 0.25*p.BS_CYCLE_BEGINNINGBALANCE then 'D - Min-25% Pay'
//            when (-1)*p.CD_NETPAYMENTS_AM < 0.5*p.BS_CYCLE_BEGINNINGBALANCE then 'E - 25-50% Pay'
//            when (-1)*p.CD_NETPAYMENTS_AM < 0.75*p.BS_CYCLE_BEGINNINGBALANCE then 'F - 50-75% Pay'
//            when (-1)*p.CD_NETPAYMENTS_AM < p.BS_CYCLE_BEGINNINGBALANCE then 'G - 75-100% Pay' 
//            else 'J - ERROR'
//          end as payment_behavior
        ,case when p.BS_MINIMUM_PAYMENT_DUE = 0 or p.BS_MINIMUM_PAYMENT_DUE is null then 0 else 1 end as payment_required

        ,port.portfolio_desc
        ,case when p.st_external_status = 'Z' then 0 else bom.FSF_NONCO_TOT_PRIN_BAL_AMT end as prin_bal_bom

        ,cmf3.payment_behavior
        
        ,cmf2.net_payments
        ,cmf2.net_late_fees_assessed
        ,cmf2.gross_late_fees_assessed
        ,cmf2.late_fee_adjustments
        ,cmf2.late_fee_waivers
        ,cmf2.gross_payments
        ,cmf2.gross_prin_pay
        ,cmf2.gross_non_prin_pay
        ,cmf2.purchase_am
        ,cmf2.cash_adv_am
        ,cmf2.returns_am
        ,cmf2.bt_am
        ,cmf2.payments_ct
        ,cmf2.purchase_ct
        ,cmf2.returns_ct
        ,cmf2.bt_ct

       
	   -- End of performance data


from 
firstdata.nonpii.account_performance_cycle_end p

-- C/O's
left join 
(SELECT 
AT_ACCOUNTID, min(AT_MONTHSONBOOKS) as chg_off_mob from firstdata.nonpii.account_performance_cycle_end where ST_EXTERNAL_STATUS = 'Z'
group by AT_ACCOUNTID
) chg on p.AT_ACCOUNTID = chg.AT_ACCOUNTID
-- Voluntary Attrition
left join 
(SELECT 
AT_ACCOUNTID, min(AT_MONTHSONBOOKS) as attr_mob from firstdata.nonpii.account_performance_cycle_end where ST_EXTERNAL_STATUS = 'C'
group by AT_ACCOUNTID
) attr on p.AT_ACCOUNTID = attr.AT_ACCOUNTID

-- Table to link offer ID to account ID
//left join firstdata.nonpii.offer_to_account_xref as j
//    on p.AT_ACCOUNTID=j.chd_account_number_scrambled 
left join FIRSTDATA.NONPII.MASTER_ACCOUNT j
      on p.AT_ACCOUNTID=j.ACCOUNTID 
-- Primary app info table
left join experian.nonpii.APPLICATION_DECISIONS as d
    on trim(ltrim(cast(d.offer_id as varchar(15)),'0')) = trim(ltrim(cast(j.offerid as varchar(15)),'0')) and d.decision_cd = 'A'
    and d.Recent_Version= '1'

-- Marketing Info
left join acxiom.nonpii.offer_mail_details m on trim(ltrim(cast(d.offer_id as varchar(15)),'0')) = trim(ltrim(cast(m.OfferID as varchar(15)),'0'))

-- Model Scores
left join USER_DB.JWANG.FSF_SCORES_MASTER_SEG ms on trim(ltrim(cast(d.offer_id as varchar(15)),'0')) = trim(ltrim(cast(ms.offer_id as varchar(15)),'0'))
left join USER_DB.JWANG.FSF_MASTER_SCRS_AS_20200229 jay on trim(ltrim(cast(d.offer_id as varchar(15)),'0')) = trim(ltrim(cast(jay.offer_id as varchar(15)),'0'))
left join UserDB.Indus.CUST_SEG_AS_PER_FIRST_BUREAU_REFRESH i on trim(ltrim(cast(d.offer_id as varchar(15)),'0')) = trim(ltrim(cast(i.offer_id as varchar(15)),'0'))

-- Lost/Stolen Info
left outer join 
    (
      select 

      a.CHD_ACCOUNT_NUMBER_SCRAMBLED
      ,b.cal_month
      ,b.data_dt
      ,a.chd_status_reason_code
      ,a.CHD_EXTERNAL_STATUS
      ,a.CHD_LAST_STATEMENTED_BAL

      from FIRSTDATA.NONPII.CARDHOLDER_MASTER a
      inner join 
          (select CHD_ACCOUNT_NUMBER_SCRAMBLED, year(data_dt)*100 + month(data_dt) as cal_month, max(data_dt) as data_dt from FIRSTDATA.NONPII.CARDHOLDER_MASTER group by 1,2) b 
          on a.CHD_ACCOUNT_NUMBER_SCRAMBLED = b.CHD_ACCOUNT_NUMBER_SCRAMBLED and a.data_dt = b.data_dt
      )
as cmf
		on cmf.CHD_ACCOUNT_NUMBER_SCRAMBLED = p.AT_ACCOUNTID and  
			cmf.cal_month  = p.AT_PERIODID		
			and p.ST_EXTERNAL_STATUS = cmf.CHD_EXTERNAL_STATUS 	

-- Better Payment Metrics
left outer join 
    (
    select
      CHD_ACCOUNT_NUMBER_SCRAMBLED as accountid
      ,cal_month
      ,sum(NET_PAYMENTS) as net_payments
      ,sum(net_late_fees_assessed) as net_late_fees_assessed
      ,sum(gross_late_fees_assessed) as gross_late_fees_assessed
      ,sum(late_fee_adjustments) as late_fee_adjustments
      ,sum(late_fee_waivers) as late_fee_waivers
      ,sum(gross_payments) as gross_payments
      ,sum(gross_prin_pay) as gross_prin_pay
      ,sum(gross_non_prin_pay) as gross_non_prin_pay
      ,sum(purchase_am) as purchase_am
      ,sum(cash_adv_am) as cash_adv_am
      ,sum(returns_am) as returns_am
      ,sum(bt_am) as bt_am
      ,sum(payments_ct) as payments_ct
      ,sum(purchase_ct) as purchase_ct
      ,sum(returns_ct) as returns_ct
      ,sum(bt_ct) as bt_ct
    from USER_DB.DTHOMAS.consolidated_payments
    group by 1,2
    )
as cmf2
		on cmf2.accountid = p.AT_ACCOUNTID and  
			cmf2.cal_month  = p.AT_PERIODID		

-- Payment Behavior
left outer join 
    (
    select
      CHD_ACCOUNT_NUMBER_SCRAMBLED as accountid
      ,cal_month
      ,payment_behavior_d as payment_behavior
    from USER_DB.DTHOMAS.consolidated_payments
      QUALIFY ROW_NUMBER() OVER (PARTITION BY CHD_ACCOUNT_NUMBER_SCRAMBLED, cal_month ORDER BY data_dt desc) = 1
    )
as cmf3
		on cmf3.accountid = p.AT_ACCOUNTID and  
			cmf3.cal_month  = p.AT_PERIODID


-- Portfolio Metrics
left join 
  (
  select 
  d.offer_ID
  ,chd.CHD_ACCOUNT_NUMBER_SCRAMBLED
  ,case when chd.chd_portfolio_no = '00000' then 'D - Unknown'
      when chd.chd_portfolio_no = '01111' then 'B - Warehouse'
      when chd.chd_portfolio_no = '02222' then 'C - FSF Equity'
      when chd.chd_portfolio_no = '03333' then 'A - Trust'
      else 'E - Missing'
    end as portfolio_desc
  ,chd.chd_portfolio_no
  from FIRSTDATA.NONPII.CARDHOLDER_MASTER chd
  left join FIRSTDATA.NONPII.MASTER_ACCOUNT j
        on chd.CHD_ACCOUNT_NUMBER_SCRAMBLED=j.ACCOUNTID 
  -- Primary app info table
  left join experian.nonpii.APPLICATION_DECISIONS as d
      on trim(ltrim(cast(d.offer_id as varchar(15)),'0')) = trim(ltrim(cast(j.offerid as varchar(15)),'0')) and d.decision_cd = 'A'
      and d.Recent_Version= '1'
  where chd.data_dt = $portfolio_asOf_dt
       and chd.CHD_EXTERNAL_STATUS not in ('Z','U','L')
   order by 1,2
  )
  port on trim(ltrim(cast(d.offer_id as varchar(15)),'0')) = trim(ltrim(cast(port.offer_id as varchar(15)),'0'))

-- BOM Principal Balances
left join USER_DB.DTHOMAS.bom_bals bom 
    on p.at_accountid = bom.CHD_ACCOUNT_NUMBER_SCRAMBLED 
    and p.at_periodid = bom.cal_month 
    and bom.CHD_EXTERNAL_STATUS not in ('Z','U','L')
//    and p.st_external_status = bom.CHD_EXTERNAL_STATUS 	

-- BOM Total Balances
left join 
  (SELECT
  AT_ACCOUNTID
  ,AT_PERIODID
  ,ST_EXTERNAL_STATUS
  ,case when ST_EXTERNAL_STATUS = 'Z' then 0 else BS_MONTH_BEGINNINGBALANCE end as BS_MONTH_BEGINNINGBALANCE
  from  
  firstdata.nonpii.account_performance_month_end
  where st_external_status not in ('Z','U','L')
  ) mth on p.AT_ACCOUNTID = mth.AT_ACCOUNTID and p.AT_PERIODID = mth.AT_PERIODID
  //    and p.st_external_status = mth.st_external_status

WHERE
	--campaigns desired
    p.ST_EXTERNAL_STATUS not in ('U','L')			
	and (	(cmf.chd_status_reason_code is null) or		
		(cmf.chd_status_reason_code<>'88' -- exclude third party fraud chargeoff		
		and p.ST_EXTERNAL_STATUS not in ('U') -- exclude stolen		
		and ((p.ST_EXTERNAL_STATUS = ('L') and cmf.CHD_LAST_STATEMENTED_BAL <> '0' ) or p.ST_EXTERNAL_STATUS not in ('L')) -- exclude lost with no balance		
		)		
	 )		
//	and d.CAMPAIGN_ID <> '89' 
//    and d.campaign_id not like '%D%' and trim(ltrim(cast(d.offer_id as varchar(15)),'0')) <> '' 
order by 1,2
;


select
cal_month
,portfolio_desc
,affiliate
,pop_seg
,mob
,appmonth
,appqtr
,floor(appmonth/100) as appyr
,case when util_score < 0.2 then 1 else 0 end as util_cut
,case when lt3_band > 13 then 1 else 0 end as lt3_cut
,case when unsec_debt > 17000 then 1 else 0 end as debt_cut
,case when product_nm like '%PLAT%' then 'A - Platinum'
    when product_nm like '%REW%' then 'B - Rewards'
    when product_nm like '%OPT%' then 'C - Optimum'
    else 'D - ERROR'
   end as product
,case when FDR_PURCH_PROMO_IN = 'A' then 0 else 1 end as promo_flag

,sum(prin_bal_bom) as prin_bal_bom
,sum(BS_CYCLE_BEGINNINGBALANCE) as cycle_beg_bal
,sum(BS_MONTH_BEGINNINGBALANCE) as month_beg_bal

,sum(net_payments) as net_payments
,sum(net_late_fees_assessed) as net_late_fees_assessed
,sum(gross_late_fees_assessed) as gross_late_fees_assessed
,sum(late_fee_adjustments) as late_fee_adjustments
,sum(late_fee_waivers) as late_fee_waivers
,sum(gross_payments) as gross_payments
,sum(gross_prin_pay) as gross_prin_pay
,sum(gross_non_prin_pay) as gross_non_prin_pay
,sum(purchase_am) as purchase_am
,sum(cash_adv_am) as cash_adv_am
,sum(returns_am) as returns_am
,sum(bt_am) as bt_am
,sum(payments_ct) as payments_ct
,sum(purchase_ct) as purchase_ct
,sum(returns_ct) as returns_ct
,sum(bt_ct) as bt_ct

,sum(open_accts) as open_accts
,sum(balance_active) as bal_act_accts

from USER_DB.DTHOMAS.cycle_end_perf
group by 1,2,3,4,5,6,7,8,9,10,11,12,13
order by 1
;

select
cal_month
,left(cal_month,4) as cal_year
,right(cal_month,2) as cal_month_only
,portfolio_desc
,mob
,floor(appmonth/100) as appyr
,case when st_external_status = 'Z' then 9
    when dq6_ct = 1 then 6
    when dq5_ct = 1 then 5
    when dq4_ct = 1 then 4
    when dq3_ct = 1 then 3
    when dq2_ct = 1 then 2
    when dq1_ct = 1 then 1
   else 0
   end as dq_bucket
   
,case when dq6_ct+dq5_ct+dq4_ct+dq3_ct+dq2_ct+dq1_ct>0 then 1 else 0 end as dq_flag
,case when payment_behavior is null and dq_flag = 1 then 'A - No Pay' else payment_behavior end as payment_behavior
,case when FDR_PURCH_PROMO_IN = 'A' then 0 else 1 end as promo_flag
,promo_time_left

,sum(prin_bal_bom) as prin_bal_bom
,sum(BS_CYCLE_BEGINNINGBALANCE) as cycle_beg_bal
,sum(BS_MONTH_BEGINNINGBALANCE) as month_beg_bal

,sum(net_payments) as net_payments
,sum(net_late_fees_assessed) as net_late_fees_assessed
,sum(gross_late_fees_assessed) as gross_late_fees_assessed
,sum(late_fee_adjustments) as late_fee_adjustments
,sum(late_fee_waivers) as late_fee_waivers
,sum(gross_payments) as gross_payments
,sum(gross_prin_pay) as gross_prin_pay
,sum(gross_non_prin_pay) as gross_non_prin_pay
,sum(purchase_am) as purchase_am
,sum(cash_adv_am) as cash_adv_am
,sum(returns_am) as returns_am
,sum(bt_am) as bt_am
,sum(payments_ct) as payments_ct
,sum(purchase_ct) as purchase_ct
,sum(returns_ct) as returns_ct
,sum(bt_ct) as bt_ct

,sum(open_accts) as open_accts
,sum(balance_active) as bal_act_accts

from USER_DB.DTHOMAS.cycle_end_perf
group by 1,2,3,4,5,6,7,8,9,10,11
order by 1


select
sum(prin_bal_bom)/1000000 as prin_bal_bom
,sum(BS_CYCLE_BEGINNINGBALANCE)/1000000 as cycle_beg_bal
,sum(BS_MONTH_BEGINNINGBALANCE)/1000000 as month_beg_bal

,sum(net_payments)/1000000 as net_payments
,sum(net_late_fees_assessed)/1000000 as net_late_fees_assessed
,sum(gross_late_fees_assessed)/1000000 as gross_late_fees_assessed
,sum(late_fee_adjustments)/1000000 as late_fee_adjustments
,sum(late_fee_waivers)/1000000 as late_fee_waivers
,sum(gross_payments)/1000000 as gross_payments
,sum(gross_prin_pay)/1000000 as gross_prin_pay
,sum(gross_non_prin_pay)/1000000 as gross_non_prin_pay
,sum(purchase_am)/1000000 as purchase_am
from USER_DB.DTHOMAS.cycle_end_perf
where cal_month = 202002

select sum(case when st_external_status not in ('L','U','Z','C') then 1 else 0 end) as open_accts
from firstdata.nonpii.account_performance_cycle_end
where at_periodid = 202002

