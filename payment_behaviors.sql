

create or replace table USER_DB.DTHOMAS.pay_types as 

SELECT 

a.CHD_ACCOUNT_NUMBER_SCRAMBLED
,a.data_dt
,year(a.data_dt)*100 + month(a.data_dt) as cal_month
,try_to_decimal(CHDpS_BILLED_PAY_DUE,14,2) AS PS_Pay_Due
,try_to_decimal(CHDPS_BALANCE,14,2) AS PS_Balance
,try_to_decimal(CHD_LS_AMT_PAYMENT,14,2) AS StmtPay
,to_date('2'||CHD_OPEN_DATE,'yyyymmdd') AS OpenDt

,CASE
WHEN PS_Pay_Due IS NULL AND stmtpay=0 THEN 'No Pay Due'
WHEN PS_Pay_Due IS NULL AND stmtpay IS null THEN 'No Pay Due'
WHEN PS_Pay_Due=0  AND stmtpay=0 THEN 'No Pay Due'
WHEN StmtPay IS NULL THEN 'No Pay'
WHEN StmtPay=0 THEN 'No Pay'
WHEN PS_Pay_Due=0  AND stmtpay>0 THEN 'Pre Pay'
WHEN stmtpay>ps_balance THEN 'More Than Balance'
WHEN stmtpay=ps_balance THEN 'Balance in Full'
WHEN stmtpay/PS_Pay_Due>=5 THEN '5x More than Min'
WHEN stmtpay/PS_Pay_Due>=4 THEN '4x More than Min'
WHEN stmtpay/PS_Pay_Due>=3 THEN '3x More than Min'
WHEN stmtpay/PS_Pay_Due>=2 THEN '2x More than Min'
WHEN stmtpay>PS_Pay_Due THEN 'More than Min'
WHEN stmtpay=PS_Pay_Due THEN 'Min Pay'
WHEN stmtpay<PS_Pay_Due THEN 'Partial Pay'
ELSE 'Error' END AS PayType

,case 
            when (PS_Pay_Due = 0 or PS_Pay_Due is null) and (stmtpay <= 0 or stmtpay is null) then 'I - No Payment Due'
            when stmtpay >= ps_balance then 'H - Full Pay'
            when stmtpay < 0 then 'AA - Neg Pay' -- compare to min pay
            when stmtpay = 0 then 'A - No Pay' -- compare to min pay
            when stmtpay < PS_Pay_Due then 'B - <Min Pay' -- compare to min pay
            when stmtpay = PS_Pay_Due then 'C - Min Pay'  -- compare to min pay
            when stmtpay < 0.25*ps_balance then 'D - Min-25% Pay'
            when stmtpay < 0.5*ps_balance then 'E - 25-50% Pay'
            when stmtpay < 0.75*ps_balance then 'F - 50-75% Pay'
            when stmtpay < ps_balance then 'G - 75-100% Pay' 
            else 'J - ERROR'
          end as payment_behavior

,1 as accts

FROM FIRSTDATA.nonpii.CARDHOLDER_MASTER a 
inner join 
          (select CHD_ACCOUNT_NUMBER_SCRAMBLED, year(data_dt)*100 + month(data_dt) as cal_month, max(data_dt) as data_dt from FIRSTDATA.NONPII.CARDHOLDER_MASTER group by 1,2) b 
          on a.CHD_ACCOUNT_NUMBER_SCRAMBLED = b.CHD_ACCOUNT_NUMBER_SCRAMBLED and a.data_dt = b.data_dt
WHERE chd_external_status NOT IN ('U','L','Z') AND CHD_STATUS_REASON_CODE<>'88'
;

//select * from USER_DB.GPERMAR.MONTHENDDATES order by 1 desc



select

a.payment_behavior as jan_pay
,b.payment_behavior as feb_pay
,c.payment_behavior as mar_pay
,d.payment_behavior as apr_pay

,case when a.CHD_ACCOUNT_NUMBER_SCRAMBLED is not null then 1 else 0 end as jan_hit
,case when b.CHD_ACCOUNT_NUMBER_SCRAMBLED is not null then 1 else 0 end as feb_hit
,case when c.CHD_ACCOUNT_NUMBER_SCRAMBLED is not null then 1 else 0 end as mar_hit
,case when d.CHD_ACCOUNT_NUMBER_SCRAMBLED is not null then 1 else 0 end as apr_hit

,count(a.CHD_ACCOUNT_NUMBER_SCRAMBLED) as accts

from USER_DB.DTHOMAS.pay_types a
left join USER_DB.DTHOMAS.pay_types b on a.CHD_ACCOUNT_NUMBER_SCRAMBLED = b.CHD_ACCOUNT_NUMBER_SCRAMBLED and b.cal_month = 202002
left join USER_DB.DTHOMAS.pay_types c on a.CHD_ACCOUNT_NUMBER_SCRAMBLED = c.CHD_ACCOUNT_NUMBER_SCRAMBLED and c.cal_month = 202003
left join USER_DB.DTHOMAS.pay_types d on a.CHD_ACCOUNT_NUMBER_SCRAMBLED = d.CHD_ACCOUNT_NUMBER_SCRAMBLED and d.cal_month = 202004

where a.cal_month = 202001

group by 1,2,3,4,5,6,7,8
;










