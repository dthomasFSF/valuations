// Create consolidated payments / transaction data from various sources
set portfolio_asOf_dt = '2020-02-19';


create or replace table USER_DB.DTHOMAS.consolidated_payments as
select 

-- Base Info from Cardholder Master

a.CHD_ACCOUNT_NUMBER_SCRAMBLED
,d.offer_ID
,d.data_dt as open_dt
,datediff(month, open_dt, a.data_dt) as mob
,ceil(mob/12) as YOB
,port.portfolio_desc as feb_port
,case when a.chd_portfolio_no = '00000' then 'D - Unknown'
    when a.chd_portfolio_no = '01111' then 'B - Warehouse'
    when a.chd_portfolio_no = '02222' then 'C - FSF Equity'
    when a.chd_portfolio_no = '03333' then 'A - Trust'
    else 'E - Missing'
  end as port_current
,case when a.data_dt < '2020-02-19' then feb_port else port_current end as portfolio_desc
,a.CHD_EXTERNAL_STATUS
,a.chd_status_reason_code
,a.chd_state as state
,year(a.data_dt)*100 + month(a.data_dt) as cal_month
,a.DATA_DT 
,(a.DATA_DT - dayofweek(a.DATA_DT) + 1) as week
,weekofyear(a.DATA_DT) as week_of_yr
,(dayofweek(a.DATA_DT) + 1) as day_of_week
,year(a.DATA_DT) as year
,month(a.DATA_DT) as cal_month_only
,TO_DATE('20'||RIGHT(NULLIF(LTRIM(a.CHD_DATE_LAST_PAYMENT,'0'),''),6),'YYYYMMDD') as last_payment_dt
,TO_DATE('20'||RIGHT(NULLIF(LTRIM(a.CHD_DATE_LAST_STMT,'0'),''),6),'YYYYMMDD') as last_stmt_dt
,try_to_decimal(CHDPS_BALANCE,14,2) AS PS_Balance
,TO_DATE('20'||RIGHT(NULLIF(LTRIM(a.CHD_PYMN_DUE_DT,'0'),''),6),'YYYYMMDD') as pay_due_dt
,try_to_decimal(CHDpS_BILLED_PAY_DUE,14,2) AS PS_Pay_Due
,(pay_due_dt - a.DATA_DT) as days_to_due_dt
,(a.DATA_DT - last_stmt_dt) as days_since_stmt

--DQ bucketing

,case when a.CHD_EXTERNAL_STATUS = 'Z' then 99
    when b.dq_status = 0 or b.dq_status is null then 0
    when b.dq_status >= 1 AND b.dq_status <= 5 then b.dq_status
    when b.dq_status >=6 then 6
    else -1
end as dq_bucket

,case when p1.dq1_ct = 1 then 1
    when p1.dq2_ct = 1 then 2
    when p1.dq3_ct = 1 then 3
    when p1.dq4_ct = 1 then 4
    when p1.dq5_ct = 1 then 5
    when p1.dq6_ct = 1 then 6
    when p1.bad = 1 then 99
  else 0 
 end as dq_bucket_prev_me
 
 -- Acquisition Promo as of Mar '20
,case when d.FDR_BT_PROMO_APR_IN = 'A' then 0
   when d.FDR_BT_PROMO_APR_IN = 'C' then 7
   when d.FDR_BT_PROMO_APR_IN = 'D' then 9
   when d.FDR_BT_PROMO_APR_IN = 'F' then 12
   when d.FDR_BT_PROMO_APR_IN = 'H' then 15
else -1 end as acq_promo_len
,case when p1.mob < acq_promo_len then 1 else 0 end as promo_active_prev_me

-- App Questions
,case when d.emp_status_cd = '1' then '1-Employed'
    when d.emp_status_cd = '2' then '2-SelfEmp'
    when d.emp_status_cd = '3' then '3-Retired/Student'
    when d.emp_status_cd = '4' then '4-Unemployed'
    when d.emp_status_cd = '5' then '5-Other'
    else '6-Error'
end as emp_status

,case when d.bank_acct_cd = '0' then '0-Blank'
    when d.bank_acct_cd = '1' then '1-Both'
    when d.bank_acct_cd = '2' then '2-Checking Only'
    when d.bank_acct_cd = '3' then '3-Savings Only'
    when d.bank_acct_cd = '4' then '4-Neither'
    else '6-Error'
end as bank_acct

,case when open_dt < '2018-04-05' AND d.invst_acct_cd = '0' then '0-Blank'
    when open_dt < '2018-04-05' AND d.invst_acct_cd = '1' then '1-Yes'
    when open_dt < '2018-04-05' AND d.invst_acct_cd = '2' then '2-No'
    when open_dt < '2018-04-05' AND d.invst_acct_cd not in ('0','1','2') then '6-Error'
    when open_dt >= '2018-04-05' then '5-Exclude'
    else '6-Error'
end as invest_acct

,case when open_dt >= '2018-04-05' AND d.invst_acct_cd = '0' then '0-Blank'
    when open_dt >= '2018-04-05' AND d.invst_acct_cd = '1' then '1-Yes'
    when open_dt >= '2018-04-05' AND d.invst_acct_cd = '2' then '2-No'
    when open_dt >= '2018-04-05' AND d.invst_acct_cd not in ('0','1','2') then '6-Error'
    when open_dt < '2018-04-05' then '5-Exclude'
    else '6-Error'
end as cash_Q

,case when d.bank_bal_cd = '0' then '0-Blank'
    when d.bank_bal_cd = '1' then '1: 0-500'
    when d.bank_bal_cd = '2' then '2: 501-1500'
    when d.bank_bal_cd = '3' then '3: 1501-5000'
    when d.bank_bal_cd = '4' then '4: 5001-10000'
    when d.bank_bal_cd = '5' then '5: 10k+'
    else '6-Error'
end as bank_bal

,case when d.rent_own_cd = '1' then '1-Rent'
    when d.rent_own_cd = '2' then '2-Own'
    when d.rent_own_cd = '3' then '3-Other'
    else '6-Error'
end as rent_v_own

-- CAP Activity
,case when cap.offerid is not null then 1 else 0 end as cap_acct
,case when cap.offerid is not null and a.data_dt >= cap.entry_date then 1 else 0 end as curr_in_cap

-- Payment Behaviors Tables
,p.payment_behavior as payment_behavior_D
,p.PayType as payment_behavior_G

-- Fin Tran Table
,sum(f.NET_PAYMENTS) as net_payments
,sum(f.net_late_fees_assessed) as net_late_fees_assessed
,sum(f.gross_late_fees_assessed) as gross_late_fees_assessed
,sum(f.late_fee_adjustments) as late_fee_adjustments
,sum(f.late_fee_waivers) as late_fee_waivers

-- Daily Transactions Table
,sum(t.PAYMENT_TOTAL) as gross_payments
,sum(t.PAYMENT_PRIN) as gross_prin_pay
,sum(t.PAYMENT_NON_PRIN) as gross_non_prin_pay
,sum(t.PURCHASE) as purchase_am
,sum(t.CASHADVANCES) as cash_adv_am
,sum(t.ALL_RETURNS) as returns_am
,sum(t.BALANCETRANSFER) as bt_am
,sum(t.NUM_OF_PAYMENT) as payments_ct
,sum(t.NUM_OF_PURCHASE) as purchase_ct
,sum(t.NUM_ALL_RETURNS) as returns_ct
,sum(t.NUM_BALANCETRANSFER) as bt_ct

,sum(case when a.chd_external_status not in ('Z','L','U','C') then 1 else 0 end) as open_accts
,sum(case when t.NUM_OF_PURCHASE > 0 and a.chd_external_status not in ('Z','L','U','C') then 1 else 0 end) as debit_active_accts

-- Payment Weighted BRMs
,sum(case when brm.Pbad_pred_brm02_sun03 <= 1 and brm.Pbad_pred_brm02_sun03 >= 0 then t.NUM_OF_PAYMENT else 0 end) as brm_pay_ct_denom 
,sum(case when brm.Pbad_pred_brm02_sun03 <= 1 and brm.Pbad_pred_brm02_sun03 >= 0 then brm.Pbad_pred_brm02_sun03*t.NUM_OF_PAYMENT else 0 end) as brm_pay_ct_num
,sum(case when brm.Pbad_pred_brm02_sun03 <= 1 and brm.Pbad_pred_brm02_sun03 >= 0 then f.NET_PAYMENTS else 0 end) as brm_pay_dol_denom 
,sum(case when brm.Pbad_pred_brm02_sun03 <= 1 and brm.Pbad_pred_brm02_sun03 >= 0 then brm.Pbad_pred_brm02_sun03*f.NET_PAYMENTS else 0 end) as brm_pay_dol_num

from FIRSTDATA.NONPII.CARDHOLDER_MASTER a
    left join USER_DB.DTHOMAS.fin_trans f on a.CHD_ACCOUNT_NUMBER_SCRAMBLED = f.accountID and a.data_dt = f.DATE_PROCESSED
    left join USER_DB.DTHOMAS.daily_transactions t on a.CHD_ACCOUNT_NUMBER_SCRAMBLED = t.accountID and a.data_dt = t.DATE_PROCESSED
    left join USER_DB.DTHOMAS.pay_types p on a.CHD_ACCOUNT_NUMBER_SCRAMBLED = p.CHD_ACCOUNT_NUMBER_SCRAMBLED and a.data_dt = p.data_dt

  left join FIRSTDATA.NONPII.MASTER_ACCOUNT j
        on a.CHD_ACCOUNT_NUMBER_SCRAMBLED=j.ACCOUNTID 
  -- Primary app info table
  left join experian.nonpii.APPLICATION_DECISIONS as d
      on trim(ltrim(cast(d.offer_id as varchar(15)),'0')) = trim(ltrim(cast(j.offerid as varchar(15)),'0')) and d.decision_cd = 'A'
      and d.Recent_Version= '1'

  left join 
    (
    select 
    d.offer_ID
    ,chd.CHD_ACCOUNT_NUMBER_SCRAMBLED
    ,case when chd.chd_portfolio_no = '00000' then 'D - Unknown'
        when chd.chd_portfolio_no = '01111' then 'B - Warehouse'
        when chd.chd_portfolio_no = '02222' then 'C - FSF Equity'
        when chd.chd_portfolio_no = '03333' then 'A - Trust'
        else 'E - Missing'
      end as portfolio_desc
    ,chd.chd_portfolio_no
    from FIRSTDATA.NONPII.CARDHOLDER_MASTER chd
    left join FIRSTDATA.NONPII.MASTER_ACCOUNT j
          on chd.CHD_ACCOUNT_NUMBER_SCRAMBLED=j.ACCOUNTID 
    -- Primary app info table
    left join experian.nonpii.APPLICATION_DECISIONS as d
        on trim(ltrim(cast(d.offer_id as varchar(15)),'0')) = trim(ltrim(cast(j.offerid as varchar(15)),'0')) and d.decision_cd = 'A'
        and d.Recent_Version= '1'
    where chd.data_dt = $portfolio_asOf_dt
         and chd.CHD_EXTERNAL_STATUS not in ('Z','U','L')
     order by 1,2
    )
    port on trim(ltrim(cast(d.offer_id as varchar(15)),'0')) = trim(ltrim(cast(port.offer_id as varchar(15)),'0'))

    left join user_db.caiken.CAP_accts cap on d.offer_id = cap.offerid
    
    left join USER_DB.DTHOMAS.brm_consolidated brm 
    on trim(ltrim(cast(d.offer_id as varchar(15)),'0')) = trim(ltrim(cast(brm.offerid as varchar(15)),'0')) 
        and trim(ltrim(cast(brm.month_end as varchar(15)),'0')) = (year(add_months(a.data_dt,-1))*100 + month(add_months(a.data_dt,-1)))
        
    left join USER_DB.DTHOMAS.account_lvl_perf p1 
    on trim(ltrim(cast(d.offer_id as varchar(15)),'0')) = trim(ltrim(cast(p1.offer_id as varchar(15)),'0')) 
        and trim(ltrim(cast(p1.cal_month as varchar(15)),'0')) = (year(add_months(a.data_dt,-1))*100 + month(add_months(a.data_dt,-1)))
    
    left join (
      
      select 
      CHD_ACCOUNT_NUMBER_SCRAMBLED
      ,data_dt
      ,TO_NUMERIC(LAG(a.CHD_DEL_NO_CYCLES,1,NULL) OVER (PARTITION BY a.CHD_ACCOUNT_NUMBER_SCRAMBLED ORDER BY a.DATA_DT)) as dq_status
      from FIRSTDATA.NONPII.CARDHOLDER_MASTER a
      
    ) b on a.CHD_ACCOUNT_NUMBER_SCRAMBLED = b.CHD_ACCOUNT_NUMBER_SCRAMBLED and a.data_dt = b.data_dt
    
where a.chd_external_status not in ('Z','U','L') and a.CHD_STATUS_REASON_CODE<>'88'

group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39
order by 1,4,5
;

select * from USER_DB.DTHOMAS.consolidated_payments limit 10

-- Standard Daily Payments Analysis
select
a.cal_month
,floor(a.cal_month/100) as cal_year
,a.week
,a.data_dt
,a.day_of_week
,a.portfolio_desc
,a.cap_acct
,a.curr_in_cap
,a.yob

,case when net_payments = 0 then 'Z - No Pay Day'
    when (PS_Pay_Due = 0 or PS_Pay_Due is null) and (net_payments <= 0 or net_payments is null) then 'I - No Payment Due'
    when net_payments >= ps_balance then 'H - Full Pay'
    when net_payments < 0 then 'AA - Neg Pay' -- compare to min pay
    when net_payments = 0 then 'A - No Pay' -- compare to min pay
    when net_payments < PS_Pay_Due then 'B - <Min Pay' -- compare to min pay
    when net_payments = PS_Pay_Due then 'C - Min Pay'  -- compare to min pay
    when net_payments < 0.25*ps_balance then 'D - Min-25% Pay'
    when net_payments < 0.5*ps_balance then 'E - 25-50% Pay'
    when net_payments < 0.75*ps_balance then 'F - 50-75% Pay'
    when net_payments < ps_balance then 'G - 75-100% Pay' 
    else 'J - ERROR'
  end as payment_behavior

//,case when state = 'NY' then '1-NY'
//    when state = 'NJ' then '2-NJ'
//    when state = 'CA' then '3-CA'
//    else '4-Other'
//end as state_grp

,sum(net_payments) as payments
,sum(payments_ct) as payments_ct
,sum(case when payment_behavior = 'H - Full Pay' then payments_ct else 0 end) as full_payment_ct
,sum(case when payments_ct > 0 then ps_balance else 0 end) as ps_balance

,sum(open_accts) as open_accts
,sum(debit_active_accts) as debit_active_accts

-- BRM & DQ Based Weightings
,sum(brm_pay_ct_denom) as brm_pay_ct_denom 
,sum(brm_pay_ct_num) as brm_pay_ct_num
,sum(brm_pay_dol_denom) as brm_pay_dol_denom 
,sum(brm_pay_dol_num) as brm_pay_dol_num
,sum(case when dq_bucket > 0 then payments_ct else 0 end) as dq_payments_ct
,sum(case when dq_bucket > 0 then net_payments else 0 end) as dq_payments_dol
,sum(dq_bucket*payments_ct) as dq_pay_ct_sum
,sum(dq_bucket*net_payments) as dq_pay_dol_sum

,sum(case when promo_active_prev_me = 1 then payments_ct else 0 end) as promo_payments_ct
,sum(case when promo_active_prev_me = 1 then net_payments else 0 end) as promo_payments_dol

,sum(case when a.cap_acct = 1 then payments_ct else 0 end) as cap_payments_ct
,sum(case when a.cap_acct = 1 then net_payments else 0 end) as cap_payments_dol

,sum((case when mob>= 1 and mob <=96 then mob else null end)*payments_ct) as mob_pay_ct_sum
,sum((case when mob>= 1 and mob <=96 then mob else null end)*net_payments) as mob_pay_dol_sum

,sum(case when a.curr_in_cap = 1 or dq_bucket > 0 then net_payments else 0 end) as dq_or_cap_payments_dol
,sum(case when dq_bucket > 0 then net_payments when (dq_bucket = 0 and a.curr_in_cap = 1) then 0.75*net_payments else 0 end) as dq_or_cap75_payments_dol

,sum(gross_payments) as gross_payments
,sum(gross_prin_pay) as gross_prin_pay
,sum(gross_non_prin_pay) as gross_non_prin_pay


from USER_DB.DTHOMAS.consolidated_payments a
where cal_month >= 201901
group by 1,2,3,4,5,6,7,8,9,10
order by 1,2,3,4


-- Replace lots of stuff w/ App Qs
select
a.cal_month
,floor(a.cal_month/100) as cal_year
,a.week
,a.cap_acct
,a.curr_in_cap
,a.yob

,emp_status
,bank_acct
,invest_acct
,cash_Q
,bank_bal
,rent_v_own

,sum(net_payments) as payments
,sum(purchase_AM) as purchases
,sum(payments_ct) as payments_ct
,sum(case when net_payments >= ps_balance then payments_ct else 0 end) as full_payment_ct
,sum(purchase_ct) as purchase_ct
,sum(cash_adv_am) as cash_adv_am
,sum(returns_am) as returns_am
,sum(bt_am) as bt_am
,sum(returns_ct) as returns_ct
,sum(bt_ct) as bt_ct
,sum(case when payments_ct > 0 then ps_balance else 0 end) as ps_balance

,sum(open_accts) as open_accts
,sum(debit_active_accts) as debit_active_accts

from USER_DB.DTHOMAS.consolidated_payments a
where cal_month >= 201801
group by 1,2,3,4,5,6,7,8,9,10,11,12
order by 1,2,3,4




-- Pull BOM Prin Bal by Facility
select
b.cal_month
,case when b.data_dt <= '2020-02-19' then port.portfolio_desc else b.portfolio_desc end as portfolio_desc
,sum(case when b.CHD_EXTERNAL_STATUS = 'Z' then 0 else b.FSF_NONCO_TOT_PRIN_BAL_AMT end) as prin_bal_bom
from USER_DB.DTHOMAS.bom_bals b

  left join FIRSTDATA.NONPII.MASTER_ACCOUNT j
        on b.CHD_ACCOUNT_NUMBER_SCRAMBLED=j.ACCOUNTID 
  -- Primary app info table
  left join experian.nonpii.APPLICATION_DECISIONS as d
      on trim(ltrim(cast(d.offer_id as varchar(15)),'0')) = trim(ltrim(cast(j.offerid as varchar(15)),'0')) and d.decision_cd = 'A'
      and d.Recent_Version= '1'

  left join 
    (
    select 
    d.offer_ID
    ,chd.CHD_ACCOUNT_NUMBER_SCRAMBLED
    ,case when chd.chd_portfolio_no = '00000' then 'D - Unknown'
        when chd.chd_portfolio_no = '01111' then 'B - Warehouse'
        when chd.chd_portfolio_no = '02222' then 'C - FSF Equity'
        when chd.chd_portfolio_no = '03333' then 'A - Trust'
        else 'E - Missing'
      end as portfolio_desc
    ,chd.chd_portfolio_no
    from FIRSTDATA.NONPII.CARDHOLDER_MASTER chd
    left join FIRSTDATA.NONPII.MASTER_ACCOUNT j
          on chd.CHD_ACCOUNT_NUMBER_SCRAMBLED=j.ACCOUNTID 
    -- Primary app info table
    left join experian.nonpii.APPLICATION_DECISIONS as d
        on trim(ltrim(cast(d.offer_id as varchar(15)),'0')) = trim(ltrim(cast(j.offerid as varchar(15)),'0')) and d.decision_cd = 'A'
        and d.Recent_Version= '1'
    where chd.data_dt = $portfolio_asOf_dt
         and chd.CHD_EXTERNAL_STATUS not in ('Z','U','L')
     order by 1,2
    )
    port on trim(ltrim(cast(d.offer_id as varchar(15)),'0')) = trim(ltrim(cast(port.offer_id as varchar(15)),'0'))

group by 1,2
order by 1,2


-- Payments by Due Date

select

pd_calmonth
,trxn_calmonth
,pd_year
,pd_month
,pay_due_dt
,case when pay_due_dt < current_date then 1 else 0 end as mature_due_dt
,dayofmonth(pay_due_dt) as pd_DoM
,portfolio_desc
,state_grp
,cap_acct
,curr_in_cap

,case when net_payments = 0 then 'Z - No Pay Day'
    when (PS_Pay_Due = 0 or PS_Pay_Due is null) and (net_payments <= 0 or net_payments is null) then 'I - No Payment Due'
    when net_payments >= ps_balance then 'H - Full Pay'
    when net_payments < 0 then 'AA - Neg Pay' -- compare to min pay
    when net_payments = 0 then 'A - No Pay' -- compare to min pay
    when net_payments < PS_Pay_Due then 'B - <Min Pay' -- compare to min pay
    when net_payments = PS_Pay_Due then 'C - Min Pay'  -- compare to min pay
    when net_payments < 0.25*ps_balance then 'D - Min-25% Pay'
    when net_payments < 0.5*ps_balance then 'E - 25-50% Pay'
    when net_payments < 0.75*ps_balance then 'F - 50-75% Pay'
    when net_payments < ps_balance then 'G - 75-100% Pay' 
    else 'J - ERROR'
  end as payment_behavior

,sum(case when payment_behavior = 'H - Full Pay' then num_accts_paying else 0 end) as full_payment_accts

,sum(ps_balance) as ps_balance
,sum(open_accts) as open_accts
,sum(debit_active_accts) as debit_active_accts
,sum(num_accts_paying) as num_accts_paying

,sum(net_payments) as net_payments
,sum(payments_ct) as payments_ct
,sum(purchases) as purchases
,sum(purchase_ct) as purchase_ct
,sum(cash_adv_am) as cash_adv_am
,sum(returns_am) as returns_am
,sum(bt_am) as bt_am
,sum(returns_ct) as returns_ct
,sum(bt_ct) as bt_ct

from (
select

a.CHD_ACCOUNT_NUMBER_SCRAMBLED

,year(a.pay_due_dt)*100 +month(a.pay_due_dt) as pd_calmonth
,a.cal_month as trxn_calmonth
,year(a.pay_due_dt) as pd_year
,month(a.pay_due_dt) as pd_month
,a.pay_due_dt
,a.portfolio_desc
,case when state = 'NY' then '1-NY'
    when state = 'NJ' then '2-NJ'
    when state = 'CA' then '3-CA'
    else '4-Other'
end as state_grp

,max(a.cap_acct) as cap_acct
,max(a.curr_in_cap) as curr_in_cap
,max(ps_balance) as ps_balance
,max(PS_Pay_Due) as PS_Pay_Due
,max(open_accts) as open_accts
,max(debit_active_accts) as debit_active_accts
,max(case when payments_ct > 0 then 1 else 0 end) as num_accts_paying

  
,sum(net_payments) as net_payments
,sum(payments_ct) as payments_ct
,sum(purchase_AM) as purchases
,sum(purchase_ct) as purchase_ct
,sum(cash_adv_am) as cash_adv_am
,sum(returns_am) as returns_am
,sum(bt_am) as bt_am
,sum(returns_ct) as returns_ct
,sum(bt_ct) as bt_ct


from USER_DB.DTHOMAS.consolidated_payments a
group by 1,2,3,4,5,6,7,8
order by 1,2,3,4,5,6
)
where pd_calmonth >= 201801
group by 1,2,3,4,5,6,7,8,9,10,11,12
order by 1,2,3,4,5,6,7,8,9,10,11,12


