

--drop table Fsf_userdb.dbo.marketing_model_scores

create table Fsf_userdb.dbo.marketing_model_scores(
OfferID char(10)
,mktg_LT int
,mktg_LT_description char(15)
,mktg_FICO int
)

select * from Fsf_userdb.dbo.marketing_model_scores


insert into Fsf_userdb.dbo.marketing_model_scores (OfferID, mktg_LT, mktg_LT_description, mktg_FICO)
select distinct a.* from (
(select m.OfferID, m.RiskScore3Ntile2 as mktg_LT, m.RiskScore3Description as mktg_LT_description, m.PrescreenFICO as mktg_FICO from Acxiom_Raw.dbo.FairSquare_201801_PreQual_ArgusRF m 
inner join experian_raw.[dbo].[RAW_APPLICATION_DECISIONS] d on m.OfferID = d.offer_ID and d.decision_cd = 'A')
union all
(select m.OfferID, m.RiskScore3Ntile2 as mktg_LT, m.RiskScore3Description as mktg_LT_description, m.PrescreenFICO as mktg_FICO from Acxiom_Raw.dbo.FairSquare_201802_PreQual_ArgusRF m 
inner join experian_raw.[dbo].[RAW_APPLICATION_DECISIONS] d on m.OfferID = d.offer_ID and d.decision_cd = 'A')
union all
(select m.OfferID, m.RiskScore3Ntile2 as mktg_LT, m.RiskScore3Description as mktg_LT_description, m.PrescreenFICO as mktg_FICO from Acxiom_Raw.dbo.FairSquare_201803_PreQual_ArgusRF m 
inner join experian_raw.[dbo].[RAW_APPLICATION_DECISIONS] d on m.OfferID = d.offer_ID and d.decision_cd = 'A')
union all
(select m.OfferID, m.RiskScore3Ntile2 as mktg_LT, m.RiskScore3Description as mktg_LT_description, m.PrescreenFICO as mktg_FICO from Acxiom_Raw.dbo.FairSquare_201804_PreQual_ArgusRF m 
inner join experian_raw.[dbo].[RAW_APPLICATION_DECISIONS] d on m.OfferID = d.offer_ID and d.decision_cd = 'A')
union all
(select m.OfferID, m.RiskScore3Ntile2 as mktg_LT, m.RiskScore3Description as mktg_LT_description, m.PrescreenFICO as mktg_FICO from Acxiom_Raw.dbo.FairSquare_201805_PreQual_ArgusRF m 
inner join experian_raw.[dbo].[RAW_APPLICATION_DECISIONS] d on m.OfferID = d.offer_ID and d.decision_cd = 'A')
union all
(select m.OfferID, m.RiskScore3Ntile2 as mktg_LT, m.RiskScore3Description as mktg_LT_description, m.PrescreenFICO as mktg_FICO from Acxiom_Raw.dbo.FairSquare_201806_PreQual_ArgusRF m 
inner join experian_raw.[dbo].[RAW_APPLICATION_DECISIONS] d on m.OfferID = d.offer_ID and d.decision_cd = 'A')
union all
(select m.OfferID, m.RiskScore3Ntile2 as mktg_LT, m.RiskScore3Description as mktg_LT_description, m.PrescreenFICO as mktg_FICO from Acxiom_Raw.dbo.FairSquare_201807_PreQual_ArgusRF m 
inner join experian_raw.[dbo].[RAW_APPLICATION_DECISIONS] d on m.OfferID = d.offer_ID and d.decision_cd = 'A')
union all
(select m.OfferID, m.RiskScore3Ntile2 as mktg_LT, m.RiskScore3Description as mktg_LT_description, m.PrescreenFICO as mktg_FICO from Acxiom_Raw.dbo.FairSquare_201808_PreQual_ArgusRF m 
inner join experian_raw.[dbo].[RAW_APPLICATION_DECISIONS] d on m.OfferID = d.offer_ID and d.decision_cd = 'A')
union all
(select m.OfferID, m.RiskScore3Ntile2 as mktg_LT, m.RiskScore3Description as mktg_LT_description, m.PrescreenFICO as mktg_FICO from Acxiom_Raw.dbo.FairSquare_201809_PreQual_ArgusRF m 
inner join experian_raw.[dbo].[RAW_APPLICATION_DECISIONS] d on m.OfferID = d.offer_ID and d.decision_cd = 'A')
union all
(select m.OfferID, m.RiskScore3Ntile2 as mktg_LT, m.RiskScore3Description as mktg_LT_description, m.PrescreenFICO as mktg_FICO from Acxiom_Raw.dbo.FairSquare_201810_PreQual_ArgusRF_20180912 m 
inner join experian_raw.[dbo].[RAW_APPLICATION_DECISIONS] d on m.OfferID = d.offer_ID and d.decision_cd = 'A')
union all
(select m.OfferID, m.RiskScore3Ntile2 as mktg_LT, m.RiskScore3Description as mktg_LT_description, m.PrescreenFICO as mktg_FICO from Acxiom_Raw.dbo.FairSquare_201811_PreQual_ArgusRF_20181012 m 
inner join experian_raw.[dbo].[RAW_APPLICATION_DECISIONS] d on m.OfferID = d.offer_ID and d.decision_cd = 'A')
union all
(select m.OfferID, m.RiskScore3Ntile2 as mktg_LT, m.RiskScore3Description as mktg_LT_description, m.PrescreenFICO as mktg_FICO from Acxiom_Raw.dbo.FairSquare_201812_PreQual_ArgusRF_20181107 m 
inner join experian_raw.[dbo].[RAW_APPLICATION_DECISIONS] d on m.OfferID = d.offer_ID and d.decision_cd = 'A')
union all
(select m.OfferID, m.RiskScore3Ntile2 as mktg_LT, m.RiskScore3Description as mktg_LT_description, m.PrescreenFICO as mktg_FICO from Acxiom_Raw.dbo.FairSquare_201901_PreQual_ArgusRF_20181212 m 
inner join experian_raw.[dbo].[RAW_APPLICATION_DECISIONS] d on m.OfferID = d.offer_ID and d.decision_cd = 'A')
union all
(select m.OfferID, m.RiskScore3Ntile2 as mktg_LT, m.RiskScore3Description as mktg_LT_description, m.PrescreenFICO as mktg_FICO from Acxiom_Raw.dbo.FairSquare_201902_PreQual_ArgusRF m 
inner join experian_raw.[dbo].[RAW_APPLICATION_DECISIONS] d on m.OfferID = d.offer_ID and d.decision_cd = 'A')
) a


select top 10 * from Fsf_userdb.dbo.marketing_model_scores

select mktg_LT, count(*) as record_count from Fsf_userdb.dbo.marketing_model_scores group by mktg_LT

select mktg_LT_description, count(*) as record_count from Fsf_userdb.dbo.marketing_model_scores group by mktg_LT_description